//
//  CoverLetterView.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 15/08/23.
//

import UIKit

class CoverLetterView: UIViewController {
    @IBOutlet var coverLetterTableView: UITableView?
    @IBOutlet var noCoverLetterView: UIView?
    var coverLetter = [CoverLetterModel]()
    let coverLetterTable = CoverLetterTable()

    override func viewDidLoad() {
        super.viewDidLoad()
        getCoverLetterAllData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if coverLetter.isEmpty {
            noCoverLetterView?.isHidden = false
            coverLetterTableView?.isHidden = true
        } else {
            noCoverLetterView?.isHidden = true
            coverLetterTableView?.isHidden = false
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    func getCoverLetterAllData() {
        coverLetter = coverLetterTable.getCoverLetterFromTable()
    }
    
    @IBAction func addCoverLetterAction() {
        self.view.endEditing(true)
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.placeHolderText = ENTER_SECTION_TITLE
        vc.errorText = PLEASE_ENTER_SECTION_TITLE
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        isCoverLetter = true
        vc.completionHandler = { text in
            self.getCoverLetterAllData()
            self.coverLetterTableView?.reloadData()
        }
        self.present(vc, animated: false, completion: nil)
    }

}

extension CoverLetterView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coverLetter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoverLetterTableViewCell", for: indexPath as IndexPath) as! CoverLetterTableViewCell
        cell.coverLetterTextLbl?.text = coverLetter[indexPath.row].name
        cell.timetLbl?.text = Utility.getFormattedDate(string: coverLetter[indexPath.row].createdAt ?? "\(Date())", formatter: "d MMM, yyyy")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "ResumeDetailsView") as? ResumeDetailsView ?? ResumeDetailsView()
        navigationController?.pushViewController(home, animated: true)
    }
}

class CoverLetterTableViewCell: UITableViewCell {
    @IBOutlet var coverLetterTextLbl: UILabel?
    @IBOutlet var timetLbl: UILabel?
    @IBOutlet var nextBtn: UIButton?
}
