//
//  CoverLetterModels.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 20/08/23.
//

import UIKit

class CoverLetterModel {
    public var localId: Int?
    public var name: String?
    public var createdAt: String?
    public var updatedAt: String?
}
