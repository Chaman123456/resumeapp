//
//  CustomView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class CustomView: UIViewController {
    @IBOutlet var customTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var customViewModel = CustomViewModel()
    var customTable = CustomTable()
    var selectedIndexPath = -1
    var customArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleStartDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = customTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = customTableView?.cellForRow(at: indexPath) as? EducationCell {
            cell.startDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func handleEndDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = customTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = customTableView?.cellForRow(at: indexPath) as? EducationCell {
            cell.endDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    func autofillFields() {
        customViewModel.getCustomData(resumeId: resume?.localId ?? zero) { success in
            if success {
               headingLbl?.text = customViewModel.custom?.sectionName
            }
        }
    }
    
    @IBAction func editCustomSectionName() {
        customViewModel.editCustomSectionName(viewController: self) {
            self.customViewModel.getCustomData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.customViewModel.custom?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreCustomAction() {
        customArray.append(1)
        customTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if customArray.count > 0 {
            customArray.remove(at: sender?.tag ?? zero)
        }
        self.customTableView?.reloadData()
    }
}

extension CustomView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customArray.count <= 0 ? 1 : customArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let employeCell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath as IndexPath) as! CustomCell
        employeCell.trashBtn?.tag = indexPath.row
        employeCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        employeCell.startDateTxtFld?.inputView = employeCell.startDatePicker
        employeCell.endDateTxtFld?.inputView = employeCell.endDatePicker
        employeCell.startDateTxtFld?.tag = indexPath.row
        employeCell.endDateTxtFld?.tag = indexPath.row
        employeCell.startDatePicker.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
        employeCell.endDatePicker.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            employeCell.startDatePicker.preferredDatePickerStyle = .wheels
            employeCell.startDatePicker.backgroundColor = .white
            employeCell.endDatePicker.preferredDatePickerStyle = .wheels
            employeCell.endDatePicker.backgroundColor = .white
        }
        employeCell.titleTxtFld?.setLeftPaddingPoints(10)
        employeCell.titleTxtFld?.setRightPaddingPoints(10)
        employeCell.cityTxtFld?.setLeftPaddingPoints(10)
        employeCell.cityTxtFld?.setRightPaddingPoints(10)
        employeCell.startDateTxtFld?.setLeftPaddingPoints(10)
        employeCell.startDateTxtFld?.setRightPaddingPoints(10)
        employeCell.endDateTxtFld?.setLeftPaddingPoints(10)
        employeCell.endDateTxtFld?.setRightPaddingPoints(10)
        if selectedIndexPath == indexPath.row {
            employeCell.expendableView?.isHidden = false
            employeCell.mainView?.borderWidth = 1
            employeCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            employeCell.expandBtn?.setImage(image, for: .normal)
            employeCell.trashBtn?.isHidden = false
            if indexPath.row == zero {
                employeCell.trashBtn?.isHidden = true
            } else {
                employeCell.trashBtn?.isHidden = false
            }
        } else {
            employeCell.expendableView?.isHidden = true
            employeCell.mainView?.borderWidth = 0
            employeCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            employeCell.expandBtn?.setImage(image, for: .normal)
            employeCell.trashBtn?.isHidden = true
        }
        return employeCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.customTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 750
        } else {
            return 71
        }
    }
}

class CustomCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var employerView : UIView?
    @IBOutlet var titleTxtFld: UITextField?
    @IBOutlet var cityTxtFld: UITextField?
    @IBOutlet var startDateTxtFld: UITextField?
    @IBOutlet var endDateTxtFld: UITextField?
    @IBOutlet var descriptionTxtView: UITextView?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: CustomView?
    var startDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    var endDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
}
