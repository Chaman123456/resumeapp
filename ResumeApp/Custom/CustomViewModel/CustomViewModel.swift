//
//  CustomViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class CustomViewModel {
    var custom : Custom?
    var customTable = CustomTable()
    
    init() {
        self.custom = Custom()
    }
    
    func getCustomData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.custom = customTable.getCustom(resumeId: "\(resumeId)")
        completion(self.custom?.localId ?? zero > zero ? true : false)
    }
    
    func editCustomSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_CUSTOM_NAME
        vc.errorText = PLEASE_ENTER_CUSTOM_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        custom?.sectionName = sectionName
        custom?.updatedAt = "\(Date())"
        customTable.updateCustomSection(custom: custom ?? Custom())
    }
}




