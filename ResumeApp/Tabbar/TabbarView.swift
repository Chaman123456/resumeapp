//
//  TabbarView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 26/03/23.
//

import UIKit

class TabbarView: UIViewController {
    @IBOutlet var selectedTabOne: UIImageView?
    @IBOutlet var selectedTabTwo: UIImageView?
    @IBOutlet var selectedTabThree: UIImageView?
    @IBOutlet var selectedTabFour: UIImageView?
    @IBOutlet var fourBtnViewView: UIView?
    @IBOutlet var addBtnViewView: UIView?
    @IBOutlet var bottomView: UIView?
    var resumeView: UIViewController?
    var coverLetterView: UIViewController?
    var templatesView: UIViewController?
    var settingsView: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        addResumeView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateTheme()
    }
    
    func updateTheme() {
    
    }
    
    func addResumeView() {
        removeAllChildViews()
        selectedTabOne?.isHidden = false
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        resumeView = storyBoard.instantiateViewController(withIdentifier: "ResumeView") as? ResumeView ?? ResumeView()
        self.add(resumeView ?? ResumeView(), frame: self.view.bounds)
        self.view.bringSubviewToFront(bottomView ?? UIView())
    }
    
    func gotoProfileView() {
        removeAllChildViews()
        selectedTabFour?.isHidden = false
        let storyBoard : UIStoryboard = UIStoryboard(name: "Settings", bundle:nil)
        settingsView = storyBoard.instantiateViewController(withIdentifier: "SettingsView") as? SettingsView ?? SettingsView()
        self.add(settingsView ?? SettingsView(), frame: self.view.bounds)
        self.view.bringSubviewToFront(bottomView ?? UIView())
    }
    
    func gotoCoverLetterView() {
        removeAllChildViews()
        selectedTabTwo?.isHidden = false
        let storyBoard : UIStoryboard = UIStoryboard(name: "CoverLetter", bundle:nil)
        coverLetterView = storyBoard.instantiateViewController(withIdentifier: "CoverLetterView") as? CoverLetterView ?? CoverLetterView()
        self.add(coverLetterView ?? CoverLetterView(), frame: self.view.bounds)
        self.view.bringSubviewToFront(bottomView ?? UIView())
    }
     
    func gotoTemplatesAction() {
        removeAllChildViews()
        selectedTabThree?.isHidden = false
        let storyBoard : UIStoryboard = UIStoryboard(name: "Templates", bundle:nil)
        templatesView = storyBoard.instantiateViewController(withIdentifier: "TemplatesView") as? TemplatesView ?? TemplatesView()
        self.add(templatesView ?? TemplatesView(), frame: self.view.bounds)
        self.view.bringSubviewToFront(bottomView ?? UIView())
    }
    
    func removeAllChildViews() {
        selectedTabOne?.isHidden = true
        selectedTabTwo?.isHidden = true
        selectedTabThree?.isHidden = true
        selectedTabFour?.isHidden = true
        resumeView?.remove()
        templatesView?.remove()
        coverLetterView?.remove()
        settingsView?.remove()
    }
    
    @IBAction func tabSelection(button: UIButton?) {
        if button?.tag ?? 0 == 0 {
            addResumeView()
        } else if button?.tag ?? 0 == 1 {
            gotoCoverLetterView()
        } else if button?.tag ?? 0 == 2 {
            gotoTemplatesAction()
        } else if button?.tag ?? 0 == 3 {
            gotoProfileView()
        }
    }
}
