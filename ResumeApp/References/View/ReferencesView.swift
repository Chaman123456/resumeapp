//
//  ReferencesView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class ReferencesView: UIViewController {
    @IBOutlet var referencesTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var referencesViewModel = ReferencesViewModel()
    var referenceTable = ReferenceTable()
    var selectedIndexPath = -1
    var referencesArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func autofillFields() {
        referencesViewModel.getReferencesData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = self.referencesViewModel.references?.sectionName
            }
        }
    }
    
    @IBAction func editReferencesSectionName() {
        referencesViewModel.editReferencesSectionName(viewController: self) {
            self.referencesViewModel.getReferencesData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.referencesViewModel.references?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreReferencesAction() {
        referencesArray.append(1)
        referencesTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if referencesArray.count > 0 {
            referencesArray.remove(at: sender?.tag ?? zero)
        }
        self.referencesTableView?.reloadData()
    }
}

extension ReferencesView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return referencesArray.count <= 0 ? 1 : referencesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let referencesCell = tableView.dequeueReusableCell(withIdentifier: "ReferencesCell", for: indexPath as IndexPath) as! ReferencesCell
        referencesCell.trashBtn?.tag = indexPath.row
        referencesCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        if selectedIndexPath == indexPath.row {
            referencesCell.expendableView?.isHidden = false
            referencesCell.mainView?.borderWidth = 1
            referencesCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            referencesCell.expandBtn?.setImage(image, for: .normal)
            referencesCell.trashBtn?.isHidden = false
            referencesCell.referencesNameTxtFld?.setLeftPaddingPoints(10)
            referencesCell.referencesNameTxtFld?.setRightPaddingPoints(10)
            referencesCell.companyTxtFld?.setLeftPaddingPoints(10)
            referencesCell.companyTxtFld?.setRightPaddingPoints(10)
            referencesCell.phoneTxtFld?.setLeftPaddingPoints(10)
            referencesCell.phoneTxtFld?.setRightPaddingPoints(10)
            referencesCell.emailTxtFld?.setLeftPaddingPoints(10)
            referencesCell.emailTxtFld?.setRightPaddingPoints(10)
            if indexPath.row == zero {
                referencesCell.trashBtn?.isHidden = true
            } else {
                referencesCell.trashBtn?.isHidden = false
            }
        } else {
            referencesCell.expendableView?.isHidden = true
            referencesCell.mainView?.borderWidth = 0
            referencesCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            referencesCell.expandBtn?.setImage(image, for: .normal)
            referencesCell.trashBtn?.isHidden = true
        }
        return referencesCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.referencesTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 420
        } else {
            return 71
        }
    }
}

class ReferencesCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var referencesView : UIView?
    @IBOutlet var referencesNameTxtFld: UITextField?
    @IBOutlet var companyTxtFld: UITextField?
    @IBOutlet var phoneTxtFld: UITextField?
    @IBOutlet var emailTxtFld: UITextField?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: ReferencesView?
}
