//
//  ReferencesViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class ReferencesViewModel {
    var references : References?
    var referenceTable = ReferenceTable()
    
    init() {
        self.references = References()
    }
    
    func getReferencesData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.references = referenceTable.getReference(resumeId: "\(resumeId)")
        completion(self.references?.localId ?? zero > zero ? true : false)
    }
    
    func editReferencesSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_REFERENCE_NAME
        vc.errorText = PLEASE_ENTER_REFERENCE_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        references?.sectionName = sectionName
        references?.updatedAt = "\(Date())"
        referenceTable.updateReferenceSection(references: references ?? References())
    }
}




