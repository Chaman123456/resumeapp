//
//  References.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import Foundation
import CoreData

class References {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var referenceFullName: String?
    public var company: String?
    public var phone: String?
    public var email: String?
    public var createdAt: String?
    public var updatedAt: String?
}
