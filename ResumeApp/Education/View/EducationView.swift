//
//  EducationView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class EducationView: UIViewController {
    @IBOutlet var educationTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var educationTable = EducationTable()
    var educationViewModel = EducationViewModel()
    var selectedIndexPath = -1
    var educationArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleStartDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = educationTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = educationTableView?.cellForRow(at: indexPath) as? EducationCell {
            cell.startDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func handleEndDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = educationTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = educationTableView?.cellForRow(at: indexPath) as? EducationCell {
            cell.endDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    func autofillFields() {
        educationViewModel.getEducationData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = self.educationViewModel.education?.sectionName
            }
        }
    }
    
    @IBAction func editEducationSectionName() {
        educationViewModel.editEducationSectionName(viewController: self) {
            self.educationViewModel.getEducationData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.educationViewModel.education?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreEducationAction() {
        educationArray.append(1)
        educationTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if educationArray.count > 0 {
            educationArray.remove(at: sender?.tag ?? zero)
        }
        self.educationTableView?.reloadData()
    }
}

extension EducationView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return educationArray.count <= 0 ? 1 : educationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let educationCell = tableView.dequeueReusableCell(withIdentifier: "EducationCell", for: indexPath as IndexPath) as! EducationCell
        educationCell.trashBtn?.tag = indexPath.row
        educationCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        educationCell.startDateTxtFld?.inputView = educationCell.startDatePicker
        educationCell.endDateTxtFld?.inputView = educationCell.endDatePicker
        educationCell.startDateTxtFld?.tag = indexPath.row
        educationCell.endDateTxtFld?.tag = indexPath.row
        educationCell.startDatePicker.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
        educationCell.endDatePicker.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            educationCell.startDatePicker.preferredDatePickerStyle = .wheels
            educationCell.startDatePicker.backgroundColor = .white
            educationCell.endDatePicker.preferredDatePickerStyle = .wheels
            educationCell.endDatePicker.backgroundColor = .white
        }
        educationCell.schoolTxtFld?.setLeftPaddingPoints(10)
        educationCell.schoolTxtFld?.setRightPaddingPoints(10)
        educationCell.degreeTxtFld?.setLeftPaddingPoints(10)
        educationCell.degreeTxtFld?.setRightPaddingPoints(10)
        educationCell.startDateTxtFld?.setLeftPaddingPoints(10)
        educationCell.startDateTxtFld?.setRightPaddingPoints(10)
        educationCell.endDateTxtFld?.setLeftPaddingPoints(10)
        educationCell.endDateTxtFld?.setRightPaddingPoints(10)
        educationCell.cityTxtFld?.setLeftPaddingPoints(10)
        educationCell.cityTxtFld?.setRightPaddingPoints(10)
        if selectedIndexPath == indexPath.row {
            educationCell.expendableView?.isHidden = false
            educationCell.mainView?.borderWidth = 1
            educationCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            educationCell.expandBtn?.setImage(image, for: .normal)
            educationCell.trashBtn?.isHidden = false
            if indexPath.row == zero {
                educationCell.trashBtn?.isHidden = true
            } else {
                educationCell.trashBtn?.isHidden = false
            }
        } else {
            educationCell.expendableView?.isHidden = true
            educationCell.mainView?.borderWidth = 0
            educationCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            educationCell.expandBtn?.setImage(image, for: .normal)
            educationCell.trashBtn?.isHidden = true
        }
        return educationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.educationTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 865
        } else {
            return 71
        }
    }
}

class EducationCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var educationView : UIView?
    @IBOutlet var schoolTxtFld: UITextField?
    @IBOutlet var degreeTxtFld: UITextField?
    @IBOutlet var startDateTxtFld: UITextField?
    @IBOutlet var endDateTxtFld: UITextField?
    @IBOutlet var cityTxtFld: UITextField?
    @IBOutlet var descriptionTxtView: UITextView?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: EducationView?
    var startDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    var endDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    
}
