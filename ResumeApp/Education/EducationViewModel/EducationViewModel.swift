//
//  EducationViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class EducationViewModel {
    let educationTable = EducationTable()
    var education: Education?
    
    init() {
        self.education = Education()
    }
    
    func getEducationData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.education = educationTable.getEducationHistory(resumeId: "\(resumeId)")
        completion(self.education?.localId ?? zero > zero ? true : false)
    }
    
    func editEducationSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = ENTER_EDUCATION_SUMMARY
        vc.errorText = ENTER_EDUCATION_SUMMARY
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        education?.sectionName = sectionName
        education?.updatedAt = "\(Date())"
        educationTable.updateEducationSection(education: education ?? Education())
    }
}




