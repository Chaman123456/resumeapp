//
//  CoursesView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class CoursesView: UIViewController {
    @IBOutlet var coursesTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var courseTable = CourseTable()
    var coursesViewModel = CoursesViewModel()
    var selectedIndexPath = -1
    var coursesArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    var myCell: CoursesCell?

    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleStartDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = coursesTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = coursesTableView?.cellForRow(at: indexPath) as? CoursesCell {
            cell.startDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func handleEndDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        let rowIndex = sender.tag
        if let indexPath = coursesTableView?.indexPathsForVisibleRows?.first(where: { $0.row == rowIndex }), let cell = coursesTableView?.cellForRow(at: indexPath) as? CoursesCell {
            cell.endDateTxtFld?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    func autofillFields() {
        coursesViewModel.getCoursesData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = self.coursesViewModel.courses?.sectionName
            }
        }
    }
    
    @IBAction func editCoursesSectionName() {
        coursesViewModel.editCoursesSectionName(viewController: self) {
            self.coursesViewModel.getCoursesData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.coursesViewModel.courses?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreEducationAction() {
        coursesArray.append(1)
        coursesTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if coursesArray.count > 0 {
            coursesArray.remove(at: sender?.tag ?? zero)
        }
        self.coursesTableView?.reloadData()
    }
}

extension CoursesView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coursesArray.count <= 0 ? 1 : coursesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let educationCell = tableView.dequeueReusableCell(withIdentifier: "CoursesCell", for: indexPath as IndexPath) as! CoursesCell
        educationCell.trashBtn?.tag = indexPath.row
        educationCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        educationCell.startDateTxtFld?.inputView = educationCell.startDatePicker
        educationCell.endDateTxtFld?.inputView = educationCell.endDatePicker
        educationCell.startDateTxtFld?.tag = indexPath.row
        educationCell.endDateTxtFld?.tag = indexPath.row
        educationCell.startDatePicker.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
        educationCell.endDatePicker.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            educationCell.startDatePicker.preferredDatePickerStyle = .wheels
            educationCell.startDatePicker.backgroundColor = .white
            educationCell.endDatePicker.preferredDatePickerStyle = .wheels
            educationCell.endDatePicker.backgroundColor = .white
        }
        if selectedIndexPath == indexPath.row {
            educationCell.expendableView?.isHidden = false
            educationCell.mainView?.borderWidth = 1
            educationCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            educationCell.expandBtn?.setImage(image, for: .normal)
            educationCell.trashBtn?.isHidden = false
            educationCell.courseTxtFld?.becomeFirstResponder()
            educationCell.courseTxtFld?.setLeftPaddingPoints(10)
            educationCell.courseTxtFld?.setRightPaddingPoints(10)
            educationCell.institutionTxtFld?.setLeftPaddingPoints(10)
            educationCell.institutionTxtFld?.setRightPaddingPoints(10)
            educationCell.startDateTxtFld?.setLeftPaddingPoints(10)
            educationCell.startDateTxtFld?.setRightPaddingPoints(10)
            educationCell.endDateTxtFld?.setLeftPaddingPoints(10)
            educationCell.endDateTxtFld?.setRightPaddingPoints(10)
            if indexPath.row == zero {
                educationCell.trashBtn?.isHidden = true
            } else {
                educationCell.trashBtn?.isHidden = false
            }
        } else {
            educationCell.expendableView?.isHidden = true
            educationCell.mainView?.borderWidth = 0
            educationCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            educationCell.expandBtn?.setImage(image, for: .normal)
            educationCell.trashBtn?.isHidden = true
        }
        return educationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.coursesTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 420
        } else {
            return 71
        }
    }
}

class CoursesCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var coursesView : UIView?
    @IBOutlet var courseTxtFld: UITextField?
    @IBOutlet var institutionTxtFld: UITextField?
    @IBOutlet var startDateTxtFld: UITextField?
    @IBOutlet var endDateTxtFld: UITextField?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: CoursesView?
    var startDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    var endDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
}

