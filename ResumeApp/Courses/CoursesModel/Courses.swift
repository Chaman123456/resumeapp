//
//  Courses.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import Foundation
import CoreData

class Courses {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var course: String?
    public var instiution: String?
    public var startDate: String?
    public var endDate: String?
    public var createdAt: String?
    public var updatedAt: String?
}
