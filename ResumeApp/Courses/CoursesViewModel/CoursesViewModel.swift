//
//  CoursesViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class CoursesViewModel {
    var courses : Courses?
    var courseTable = CourseTable()
    
    init() {
        self.courses = Courses()
    }
    
    func getCoursesData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.courses = courseTable.getCourses(resumeId: "\(resumeId)")
        completion(self.courses?.localId ?? zero > zero ? true : false)
    }
    
    func editCoursesSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_COURSE_NAME
        vc.errorText = PLEASE_ENTER_COURSE_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        courses?.sectionName = sectionName
        courses?.updatedAt = "\(Date())"
        courseTable.updateCoursesSection(courses: courses ?? Courses())
    }
}




