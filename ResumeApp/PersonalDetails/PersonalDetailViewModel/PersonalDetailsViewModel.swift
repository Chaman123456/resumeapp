//
//  PersonalDetailsViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 01/09/23.
//

import Foundation
import UIKit

class PersonalDetailsViewModel {
    let personalDetailsTable = PersonalDetailsTable()
    var personalDetails: PersonalDetails?
    
    init() {
        self.personalDetails = PersonalDetails()
    }
    
    func getPersonalDetailsData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.personalDetails = personalDetailsTable.getPersonalDetailsFromTable(resumeId: "\(resumeId)")
        completion(self.personalDetails?.localId ?? zero > zero ? true : false)
    }
    
    func editPersonalDetailsSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.placeHolderText = PLEASE_ENTER_SECTION_TITLE
        vc.errorText = PLEASE_ENTER_SECTION_TITLE
        vc.buttonTitle = UPDATE_TEXT
        vc.existingTextValue = personalDetails?.sectionName ?? empty
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        personalDetails?.sectionName = sectionName
        personalDetails?.updatedAt = "\(Date())"
        personalDetailsTable.updateSection(personalDetails: personalDetails ?? PersonalDetails())
    }
}

