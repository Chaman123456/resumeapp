//
//  PersonalDetails.swift
//  ResumeApp
//
//  Created by Anviam on 24/08/23.
//

import Foundation

class PersonalDetails {
    public var localId: Int?
    public var resumeId: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var sectionName: String?
    public var jobTitle: String?
    public var photo: String?
    public var firstName: String?
    public var lastName: String?
    public var email: String?
    public var phone: String?
    public var country: String?
    public var city: String?
    public var address: String?
    public var postalCode: String?
    public var nationality: String?
    public var dob: String?
}
