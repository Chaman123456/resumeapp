//
//  PersonalDetailsView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 26/08/23.
//

import UIKit
import Toast_Swift

class PersonalDetailsView: UIViewController, UITextFieldDelegate {
    @IBOutlet var jobTitle: UITextField?
    @IBOutlet var firstName: UITextField?
    @IBOutlet var lastName: UITextField?
    @IBOutlet var email: UITextField?
    @IBOutlet var phone: UITextField?
    @IBOutlet var country: UITextField?
    @IBOutlet var city: UITextField?
    @IBOutlet var address: UITextField?
    @IBOutlet var postalCode: UITextField?
    @IBOutlet var nationality: UITextField?
    @IBOutlet var dob: UITextField?
    @IBOutlet var jobTitleErrLbl: UILabel?
    @IBOutlet var firstNameErrLbl: UILabel?
    @IBOutlet var emailErrLbl: UILabel?
    @IBOutlet var phoneErrLbl: UILabel?
    @IBOutlet var countryErrLbl: UILabel?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var cityErrLbl: UILabel?
    @IBOutlet var scrollView: UIScrollView?
    var resume: Resume?
    var personalDetailsViewModel = PersonalDetailsViewModel()
    var completionHandler: (String) -> Void = {_ in}
    private lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        autofillDetails()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        dob?.text = dateFormatter.string(from: sender.date)
    }
    
    func autofillDetails() {
        personalDetailsViewModel.getPersonalDetailsData(resumeId: resume?.localId ?? zero) { success in
            if success {
                jobTitle?.text = personalDetailsViewModel.personalDetails?.jobTitle
                firstName?.text = personalDetailsViewModel.personalDetails?.firstName
                lastName?.text = personalDetailsViewModel.personalDetails?.lastName
                email?.text = personalDetailsViewModel.personalDetails?.email
                phone?.text = personalDetailsViewModel.personalDetails?.phone
                country?.text = personalDetailsViewModel.personalDetails?.country
                city?.text = personalDetailsViewModel.personalDetails?.city
                address?.text = personalDetailsViewModel.personalDetails?.address
                postalCode?.text = personalDetailsViewModel.personalDetails?.postalCode
                nationality?.text = personalDetailsViewModel.personalDetails?.nationality
                dob?.text = personalDetailsViewModel.personalDetails?.dob
                self.headingLbl?.text = personalDetailsViewModel.personalDetails?.sectionName
            }
        }
    }
    
    func setUI() {
        scrollView?.contentInset = UIEdgeInsets(top: 13, left: 00, bottom: 0, right: 0);
        jobTitle?.becomeFirstResponder()
        jobTitle?.setLeftPaddingPoints(10)
        jobTitle?.setRightPaddingPoints(10)
        firstName?.setLeftPaddingPoints(10)
        firstName?.setRightPaddingPoints(10)
        lastName?.setLeftPaddingPoints(10)
        lastName?.setRightPaddingPoints(10)
        email?.setLeftPaddingPoints(10)
        email?.setRightPaddingPoints(10)
        phone?.setLeftPaddingPoints(10)
        phone?.setRightPaddingPoints(10)
        country?.setLeftPaddingPoints(10)
        country?.setRightPaddingPoints(10)
        city?.setLeftPaddingPoints(10)
        city?.setRightPaddingPoints(10)
        address?.setLeftPaddingPoints(10)
        address?.setRightPaddingPoints(10)
        postalCode?.setLeftPaddingPoints(10)
        postalCode?.setRightPaddingPoints(10)
        nationality?.setLeftPaddingPoints(10)
        nationality?.setRightPaddingPoints(10)
        dob?.setLeftPaddingPoints(10)
        dob?.setRightPaddingPoints(10)
        dob?.inputView = datePicker
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.backgroundColor = .white
        }
    }
    
    func savePersonalDetails() {
        personalDetailsViewModel.personalDetails?.resumeId = "\(resume?.localId ?? zero)"
        personalDetailsViewModel.personalDetails?.jobTitle = jobTitle?.text
        personalDetailsViewModel.personalDetails?.firstName = firstName?.text
        personalDetailsViewModel.personalDetails?.lastName = lastName?.text
        personalDetailsViewModel.personalDetails?.email = email?.text
        personalDetailsViewModel.personalDetails?.phone = phone?.text
        personalDetailsViewModel.personalDetails?.country = country?.text
        personalDetailsViewModel.personalDetails?.city = city?.text
        personalDetailsViewModel.personalDetails?.address = address?.text
        personalDetailsViewModel.personalDetails?.postalCode = postalCode?.text
        personalDetailsViewModel.personalDetails?.nationality = nationality?.text
        personalDetailsViewModel.personalDetails?.dob = dob?.text
        personalDetailsViewModel.personalDetails?.updatedAt = "\(Date())"
        personalDetailsViewModel.personalDetailsTable.updatePersonalDetails(personalDetails: personalDetailsViewModel.personalDetails ?? PersonalDetails())
        let resumeTable = ResumeTable()
        resumeTable.updateUpdatedDate(updatedDate: "\(Date())", resumeId: resume?.localId ?? zero)
        self.navigationController?.popViewController(animated: true)
        SceneDelegate.getSceneDelegate().window?.makeToast(DATA_SAVED_TEXT)
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editSectionNameAction() {
        self.view.endEditing(true)
        personalDetailsViewModel.editPersonalDetailsSectionName(viewController: self) {
            self.headingLbl?.text = self.personalDetailsViewModel.personalDetails?.sectionName
        }
    }
    
    @IBAction func saveAction() {
        self.view.endEditing(true)
        if jobTitle?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: jobTitle, errorLabel: jobTitleErrLbl, errorText: PLEASE_ENTER_JOBTITLE)
            return
        } else if firstName?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: firstName, errorLabel: firstNameErrLbl, errorText: PLEASE_ENTER_FIRSTNAME)
            return
        } else if email?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: email, errorLabel: emailErrLbl, errorText: PLEASE_ENTER_EMAIL)
            return
        } else if !(email?.text?.isEmpty ?? true) && !Validation.isValidEmail(email?.text ?? empty ) {
            Utility.setErrorTextField(isError: true, view: email, errorLabel: emailErrLbl, errorText: PLEASE_ENTER_VALID_EMAIL)
            return
        } else if phone?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: phone, errorLabel: phoneErrLbl, errorText: PLEASE_ENTER_PHONENUMBER)
            return
        } else if !(phone?.text?.isEmpty ?? true) && !Validation.phoneNumberValidate(value: phone?.text ?? empty) {
            Utility.setErrorTextField(isError: true, view: phone, errorLabel: phoneErrLbl, errorText: PLEASE_ENTER_VALID_PHONE)
            return
        } else if country?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: country, errorLabel: countryErrLbl, errorText: PLEASE_ENTER_COUNTRY)
            return
        } else if city?.text?.isEmpty ?? false {
            Utility.setErrorTextField(isError: true, view: city, errorLabel: cityErrLbl, errorText: PLEASE_ENTER_CITY)
            return
        } else {
            savePersonalDetails()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == jobTitle {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: jobTitleErrLbl)
            return true
        } else if textField == firstName {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: firstNameErrLbl)
            return true
        } else if textField == email {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: emailErrLbl)
            return true
        } else if textField == phone {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: phoneErrLbl)
            return true
        } else if textField == country {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: countryErrLbl)
            return true
        } else if textField == city {
            Utility.setErrorTextField(isError: false, view: textField, errorLabel: cityErrLbl)
            return true
        } else {
            return false
        }
    }
}
