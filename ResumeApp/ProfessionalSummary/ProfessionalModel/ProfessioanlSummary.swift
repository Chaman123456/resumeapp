//
//  ProfessioanlSummary.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 29/08/23.
//

import Foundation

class ProfessionalSummary {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var summary: String?
    public var createdAt: String?
    public var updatedAt: String?
}
