//
//  ProfessionalViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 01/09/23.
//

import Foundation
import UIKit

class ProfessionalViewModel {
    let professionalSummaryTable = ProfessionalSummaryTable()
    var professionalSummary: ProfessionalSummary?
    
    init() {
        self.professionalSummary = ProfessionalSummary()
    }
    
    func getProfessioanlSummaryData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.professionalSummary = professionalSummaryTable.getProfessionalSummary(resumeId: "\(resumeId)")
        completion(self.professionalSummary?.localId ?? zero > zero ? true : false)
    }
    
    func editProfessioanlSummarySectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.errorText = ENTER_SUMMARY
        vc.buttonTitle = SAVE_TEXT
        vc.existingTextValue = professionalSummary?.sectionName ?? empty
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        professionalSummary?.sectionName = sectionName
        professionalSummary?.updatedAt = "\(Date())"
        professionalSummaryTable.updateProfessionalSection(professionalSummary: professionalSummary ?? ProfessionalSummary())
    }
}


