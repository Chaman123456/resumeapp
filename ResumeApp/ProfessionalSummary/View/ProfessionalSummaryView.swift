//
//  ProfessionalSummaryView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 29/08/23.
//

import UIKit
import Toast_Swift

class ProfessionalSummaryView: UIViewController, UITextViewDelegate {
    @IBOutlet var summarytextView: UITextView?
    @IBOutlet var summaryView: UIView?
    @IBOutlet var textCountLbl: UILabel?
    @IBOutlet var summaryErrorLbl: UILabel?
    @IBOutlet var headingLbl: UILabel?
    var resume: Resume?
    var professionalViewModel = ProfessionalViewModel()
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        summarytextView?.delegate = self
        autoFillprofessionalSummaryData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        Utility.setErrorTextField(isError: false, view: summaryView, errorLabel: summaryErrorLbl)
        if (textView == summarytextView) {
            let charsInTextView = (summarytextView?.text.count ?? 0)
            let remainingChars = charsInTextView
            if (text == "\n") {
                let remainingChars = (summarytextView?.text.count ?? 0)
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            if (text != "\n"){
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            if textView.text.count > 50 {
                textCountLbl?.text = "\(remainingChars)/200"
            } else if textView.text.count < 50 {
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            return true
        } else {
            return false
        }
    }
    
    func autoFillprofessionalSummaryData() {
        professionalViewModel.getProfessioanlSummaryData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = professionalViewModel.professionalSummary?.sectionName
                summarytextView?.text = professionalViewModel.professionalSummary?.summary
                textCountLbl?.text = "\(summarytextView?.text.count ?? zero)/50+"
            }
        }
    }
    
    func saveSummaryData() {
        self.view.endEditing(true)
        professionalViewModel.professionalSummary?.summary = summarytextView?.text ?? empty
        professionalViewModel.professionalSummary?.updatedAt = "\(Date())"
        professionalViewModel.professionalSummaryTable.updateProfessionalSummary(professionalSummary: professionalViewModel.professionalSummary ?? ProfessionalSummary())
        let resumeTable = ResumeTable()
        resumeTable.updateUpdatedDate(updatedDate: "\(Date())", resumeId: resume?.localId ?? zero)
        self.navigationController?.popViewController(animated: true)
        SceneDelegate.getSceneDelegate().window?.makeToast(DATA_SAVED_TEXT)
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveSummary() {
        if summarytextView?.text == empty {
            Utility.setErrorTextField(isError: true, view: summaryView, errorLabel: summaryErrorLbl, errorText: ENTER_SUMMARY)
            return
        }
        saveSummaryData()
    }
    
    @IBAction func editProfessionalSectionName() {
        self.view.endEditing(true)
        professionalViewModel.editProfessioanlSummarySectionName(viewController: self, completion: {
            self.professionalViewModel.getProfessioanlSummaryData(resumeId: self.resume?.localId ?? zero, completion: { success in
                self.headingLbl?.text = self.professionalViewModel.professionalSummary?.sectionName
            })
        })
    }
}
