//
//  ResumeViewModel.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 20/08/23.
//

import UIKit

class ResumeViewModel {
    let resumeTable = ResumeTable()
    var resumes: [Resume]?
    
    init() {
        self.resumes = [Resume]()
    }
    
    func getResumes(completion: ((Bool) -> Void)) {
        self.resumes = resumeTable.getResumeFromTable()
        completion(self.resumes?.isEmpty ?? false ? true : false)
    }
    
    func addResume(controller: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = ENTER_RESUME_NAME
        vc.errorText = PLEASE_ENTER_RESUME_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { resumeName in
            self.saveResumeAction(resumeName: resumeName)
            self.resumes = self.resumeTable.getResumeFromTable()
            completion()
        }
        controller.present(vc, animated: false, completion: nil)
    }
    
    func saveResumeAction(resumeName: String) {
        let resume = Resume()
        resume.resumeName = resumeName
        resume.createdAt = "\(Date())"
        let resumeId = resumeTable.saveResumeName(resume: resume)
        if resumeId != zero {
            // Save PersonalDetails
            let personalDetailsTable = PersonalDetailsTable()
            let personalDetails = PersonalDetails()
            personalDetails.createdAt = "\(Date())"
            personalDetails.sectionName = ResumeDetailSection.personalDetails.resumeSectionDefaultName()
            personalDetails.resumeId = "\(resumeId)"
            let personalDetailId = personalDetailsTable.savePersonalDetails(personalDetails: personalDetails)
            
            // Save ProfessioanlSummary
            let professionalSummaryTable = ProfessionalSummaryTable()
            let professionalSummary = ProfessionalSummary()
            professionalSummary.createdAt = "\(Date())"
            professionalSummary.resumeId = "\(resumeId)"
            professionalSummary.sectionName = ResumeDetailSection.professionalSummary.resumeSectionDefaultName()
            let professionalId = professionalSummaryTable.saveProfessionalSummary(professionalSummary: professionalSummary)
            
            // Save Experience
            let experienceSummaryTable = ExperienceSummaryTable()
            let experience = Experience()
            experience.createdAt = "\(Date())"
            experience.resumeId = "\(resumeId)"
            experience.sectionName = ResumeDetailSection.experienceSummary.resumeSectionDefaultName()
            let experienceId = experienceSummaryTable.saveExperienceSummary(experience: experience)
            
            // Save EmploymentHistory
            let employerTable = EmployerTable()
            let employer = Employer()
            employer.createdAt = "\(Date())"
            employer.resumeId = "\(resumeId)"
            employer.sectionName = ResumeDetailSection.employer.resumeSectionDefaultName()
            let employeId = employerTable.saveEmploymentHistory(employer: employer)
            
            // Save education
            let educationTable = EducationTable()
            let education = Education()
            education.createdAt = "\(Date())"
            education.resumeId = "\(resumeId)"
            education.sectionName = ResumeDetailSection.education.resumeSectionDefaultName()
            let educationId = educationTable.saveEducationHistory(education: education)
            
            // Save social Links
            let websiteSocialLinkTable = WebsiteSocialLinkTable()
            let websiteSocialLinks = WebsiteSocialLinks()
            websiteSocialLinks.createdAt = "\(Date())"
            websiteSocialLinks.resumeId = "\(resumeId)"
            websiteSocialLinks.sectionName = ResumeDetailSection.websiteAndSocialLink.resumeSectionDefaultName()
            let linksId = websiteSocialLinkTable.saveWebsiteSocialLinkTable(websiteSocialLinks: websiteSocialLinks)
            
            // Save languages
            let languageTable = LanguageTable()
            let languages = Languages()
            languages.createdAt = "\(Date())"
            languages.resumeId = "\(resumeId)"
            languages.sectionName = ResumeDetailSection.language.resumeSectionDefaultName()
            let languageId = languageTable.saveLanguages(languages: languages)
            
            // Save Reference
            let referenceTable = ReferenceTable()
            let references = References()
            references.createdAt = "\(Date())"
            references.resumeId = "\(resumeId)"
            references.sectionName = ResumeDetailSection.reference.resumeSectionDefaultName()
            let referencesId = referenceTable.saveReference(references: references)
            
            // Save Cource
            let courseTable = CourseTable()
            let courses = Courses()
            courses.createdAt = "\(Date())"
            courses.resumeId = "\(resumeId)"
            courses.sectionName = ResumeDetailSection.course.resumeSectionDefaultName()
            let coursesId = courseTable.saveCourses(courses: courses)
            
            // Save Projects
            let projectTable = ProjectTable()
            let project = Project()
            project.createdAt = "\(Date())"
            project.resumeId = "\(resumeId)"
            project.sectionName = ResumeDetailSection.project.resumeSectionDefaultName()
            let projectId = projectTable.saveProject(project: project)
            
            // Save custom
            let customTable = CustomTable()
            let custom = Custom()
            custom.createdAt = "\(Date())"
            custom.resumeId = "\(resumeId)"
            custom.sectionName = ResumeDetailSection.custom.resumeSectionDefaultName()
            let customId = customTable.saveCustom(custom: custom)
            
//            // Save skill
//            let projectTable = ProjectTable()
//            let project = Project()
//            project.createdAt = "\(Date())"
//            project.resumeId = "\(resumeId)"
//            project.sectionName = ResumeDetailSection.project.resumeSectionDefaultName()
//            let projectId = projectTable.saveProject(project: project)
            
            resumeTable.updateDetail(personalId: "\(personalDetailId)", professionalId: "\(professionalId)", employerId: "\(employeId)", resumeId: resumeId, experienceId: "\(experienceId)", educationId: "\(educationId)", websiteAndSocialLinkId: "\(linksId)", languageId: "\(languageId)", referencesId: "\(referencesId)", courseId: "\(coursesId)", projectId: "\(projectId)", skillId: "", customId: "\(customId)")
        }
    }
}
