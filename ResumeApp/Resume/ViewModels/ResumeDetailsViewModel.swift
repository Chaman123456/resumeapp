//
//  ResumeDetailsViewModel.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 21/08/23.
//

import UIKit
import Toast_Swift

enum ResumeDetailSection: String, CaseIterable {
    case personalDetails = "personal_details"
    case professionalSummary = "professional_summary"
    case experienceSummary = "experience_summary"
    case employer = "employer"
    case education = "education"
    case websiteAndSocialLink = "website_social_links"
    case project = "project"
    case skill = "skill"
    case language = "language"
    case reference = "reference"
    case course = "course"
    case custom = "custom"
    
    func resumeSectionDefaultName() -> String {
        switch self {
        case .personalDetails:
            return "Personal Details"
        case .professionalSummary:
            return "Professional Summary"
        case .experienceSummary:
            return "Experience Summary"
        case .employer:
            return "Employer"
        case .education:
            return "Education"
        case .websiteAndSocialLink:
            return "Website and Social Links"
        case .project:
            return "Project"
        case .skill:
            return "Skill"
        case .language:
            return "Language"
        case .reference:
            return "Reference"
        case .course:
            return "Course"
        case .custom:
            return "Custom"
        }
    }
}

class ResumeDetailsViewModel {
    var resumeSectionArray: [ResumeDetailSection]?
    let personalDetailsTable = PersonalDetailsTable()
    var resumeTable = ResumeTable()
    var resume: Resume?

    init() {
        self.resumeSectionArray = [ResumeDetailSection]()
    }
    
    func selectMenu(index: Int) {
        switch index {
        case 1:
            print("edit")
        case 2:
            print("delete")
        case 3:
            print("duplicate")
        default:
            break
        }
    }
    
    func getAllSection(completion: ((Bool) -> Void)) {
        if !(resume?.personalDetails ?? empty).isEmpty {
            self.resumeSectionArray?.append(.personalDetails)
        }
        if !(resume?.professionalSummary ?? empty).isEmpty {
            self.resumeSectionArray?.append(.professionalSummary)
        }
        if !(resume?.experienceSummary ?? empty).isEmpty {
            self.resumeSectionArray?.append(.experienceSummary)
        }
        if !(resume?.employer ?? empty).isEmpty {
            self.resumeSectionArray?.append(.employer)
        }
        if !(resume?.education ?? empty).isEmpty {
            self.resumeSectionArray?.append(.education)
        }
        if !(resume?.websiteAndSocialLink ?? empty).isEmpty {
            self.resumeSectionArray?.append(.websiteAndSocialLink)
        }
        if !(resume?.project ?? empty).isEmpty {
            self.resumeSectionArray?.append(.project)
        }
        if !(resume?.skill ?? empty).isEmpty {
            self.resumeSectionArray?.append(.skill)
        }
        if !(resume?.language ?? empty).isEmpty {
            self.resumeSectionArray?.append(.language)
        }
        if !(resume?.reference ?? empty).isEmpty {
            self.resumeSectionArray?.append(.reference)
        }
        if !(resume?.course ?? empty).isEmpty {
            self.resumeSectionArray?.append(.course)
        }
        if !(resume?.custom ?? empty).isEmpty {
            self.resumeSectionArray?.append(.custom)
        }
        completion(true)
    }
    
    func deleteResume(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let isDeleted = resumeTable.deleteResumeData(resumeId: resume?.localId ?? zero)
        if isDeleted {
            viewController.navigationController?.popViewController(animated: true)
            SceneDelegate.getSceneDelegate().window?.makeToast(RESUME_DELETED_SUCCESSFULLY)
            completion()
        } else {
            SceneDelegate.getSceneDelegate().window?.makeToast(SOMETHING_WENT_WRONG)
        }
    }
    
    func editResumeName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = ENTER_RESUME_NAME
        vc.errorText = PLEASE_ENTER_RESUME_NAME
        vc.buttonTitle = UPDATE_TEXT
        vc.existingTextValue = self.resume?.resumeName ?? empty
        vc.completionHandler = { resumeName in
            self.resumeTable.updateResumeName(resumeName: resumeName, resumeId: self.resume?.localId ?? zero)
            self.resume?.resumeName = resumeName
             completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func addDuplicateResume(controller: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = ENTER_RESUME_NAME
        vc.errorText = PLEASE_ENTER_RESUME_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { resumeName in
            self.saveResumeAction(resumeName: resumeName)
            completion()
        }
        controller.present(vc, animated: false, completion: nil)
    }
    
    func saveResumeAction(resumeName: String) {
        let resume = Resume()
        resume.resumeName = resumeName
        resume.createdAt = "\(Date())"
        let resumeId = resumeTable.saveResumeName(resume: resume)
        if resumeId != zero {
            // Save PersonalDetails
            let personalDetailsTable = PersonalDetailsTable()
            let personalDetails = PersonalDetails()
            personalDetails.createdAt = "\(Date())"
            personalDetails.sectionName = ResumeDetailSection.personalDetails.resumeSectionDefaultName()
            personalDetails.resumeId = "\(resumeId)"
            let personalDetailId = personalDetailsTable.savePersonalDetails(personalDetails: personalDetails)
            
            // Save ProfessioanlSummary
            let professionalSummaryTable = ProfessionalSummaryTable()
            let professionalSummary = ProfessionalSummary()
            professionalSummary.createdAt = "\(Date())"
            professionalSummary.resumeId = "\(resumeId)"
            professionalSummary.sectionName = ResumeDetailSection.professionalSummary.resumeSectionDefaultName()
            let professionalId = professionalSummaryTable.saveProfessionalSummary(professionalSummary: professionalSummary)
            
            // Save Experience
            let experienceSummaryTable = ExperienceSummaryTable()
            let experience = Experience()
            experience.createdAt = "\(Date())"
            experience.resumeId = "\(resumeId)"
            experience.sectionName = ResumeDetailSection.experienceSummary.resumeSectionDefaultName()
            let experienceId = experienceSummaryTable.saveExperienceSummary(experience: experience)
            
            // Save EmploymentHistory
            let employerTable = EmployerTable()
            let employer = Employer()
            employer.createdAt = "\(Date())"
            employer.resumeId = "\(resumeId)"
            employer.sectionName = ResumeDetailSection.employer.resumeSectionDefaultName()
            let employeId = employerTable.saveEmploymentHistory(employer: employer)
            
            // Save education
            let educationTable = EducationTable()
            let education = Education()
            education.createdAt = "\(Date())"
            education.resumeId = "\(resumeId)"
            education.sectionName = ResumeDetailSection.education.resumeSectionDefaultName()
            let educationId = educationTable.saveEducationHistory(education: education)
            
            // Save social Links
            let websiteSocialLinkTable = WebsiteSocialLinkTable()
            let websiteSocialLinks = WebsiteSocialLinks()
            websiteSocialLinks.createdAt = "\(Date())"
            websiteSocialLinks.resumeId = "\(resumeId)"
            websiteSocialLinks.sectionName = ResumeDetailSection.websiteAndSocialLink.resumeSectionDefaultName()
            let linksId = websiteSocialLinkTable.saveWebsiteSocialLinkTable(websiteSocialLinks: websiteSocialLinks)
            
            // Save languages
            let languageTable = LanguageTable()
            let languages = Languages()
            languages.createdAt = "\(Date())"
            languages.resumeId = "\(resumeId)"
            languages.sectionName = ResumeDetailSection.language.resumeSectionDefaultName()
            let languageId = languageTable.saveLanguages(languages: languages)
            
            // Save Reference
            let referenceTable = ReferenceTable()
            let references = References()
            references.createdAt = "\(Date())"
            references.resumeId = "\(resumeId)"
            references.sectionName = ResumeDetailSection.reference.resumeSectionDefaultName()
            let referencesId = referenceTable.saveReference(references: references)
            
            // Save Cource
            let courseTable = CourseTable()
            let courses = Courses()
            courses.createdAt = "\(Date())"
            courses.resumeId = "\(resumeId)"
            courses.sectionName = ResumeDetailSection.course.resumeSectionDefaultName()
            let coursesId = courseTable.saveCourses(courses: courses)
            
            // Save Projects
            let projectTable = ProjectTable()
            let project = Project()
            project.createdAt = "\(Date())"
            project.resumeId = "\(resumeId)"
            project.sectionName = ResumeDetailSection.project.resumeSectionDefaultName()
            let projectId = projectTable.saveProject(project: project)
            
            // Save custom
            let customTable = CustomTable()
            let custom = Custom()
            custom.createdAt = "\(Date())"
            custom.resumeId = "\(resumeId)"
            custom.sectionName = ResumeDetailSection.custom.resumeSectionDefaultName()
            let customId = customTable.saveCustom(custom: custom)
            
//            // Save skill
//            let projectTable = ProjectTable()
//            let project = Project()
//            project.createdAt = "\(Date())"
//            project.resumeId = "\(resumeId)"
//            project.sectionName = ResumeDetailSection.project.resumeSectionDefaultName()
//            let projectId = projectTable.saveProject(project: project)
            
            resumeTable.updateDetail(personalId: "\(personalDetailId)", professionalId: "\(professionalId)", employerId: "\(employeId)", resumeId: resumeId, experienceId: "\(experienceId)", educationId: "\(educationId)", websiteAndSocialLinkId: "\(linksId)", languageId: "\(languageId)", referencesId: "\(referencesId)", courseId: "\(coursesId)", projectId: "\(projectId)", skillId: "", customId: "\(customId)")
        }
    }
   
}
