//
//  ResumeModel.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 18/08/23.
//

import Foundation
import CoreData

class Resume {
    public var localId: Int?
    public var resumeName: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var personalDetails: String?
    public var professionalSummary: String?
    public var experienceSummary: String?
    public var employer: String?
    public var education: String?
    public var websiteAndSocialLink: String?
    public var project: String?
    public var skill: String?
    public var language: String?
    public var reference: String?
    public var course: String?
    public var custom: String?  
}
