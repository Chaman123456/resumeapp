//
//  ResumeHelperView.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 16/08/23.
//

import UIKit
import Toast_Swift

class CreateResumeAlert: UIViewController, UITextFieldDelegate {
    @IBOutlet var createResumeTxtFld: UITextField?
    @IBOutlet var resumeErrorLbl: UILabel?
    @IBOutlet var createButton: UIButton?
    var completionHandler: (String) -> Void = {_ in}
    var errorText: String?
    var placeHolderText: String?
    var buttonTitle: String?
    var existingTextValue: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    func setupUI() {
        createResumeTxtFld?.text = existingTextValue
        createButton?.setTitle(buttonTitle, for: UIControl.State.normal)
        createResumeTxtFld?.placeholder = placeHolderText ?? empty
        errorText = errorText ?? empty
        createResumeTxtFld?.becomeFirstResponder()
        createResumeTxtFld?.setLeftPaddingPoints(10)
        createResumeTxtFld?.setRightPaddingPoints(10)
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: false)
    }
    
    @IBAction func crossAction() {
        handleTap()
    }
    
    @IBAction func createAction() {
        if createResumeTxtFld?.text == empty {
            Utility.setErrorTextField(isError: true, view: createResumeTxtFld, errorLabel: resumeErrorLbl, errorText: errorText ?? empty)
            return
        }
        completionHandler(createResumeTxtFld?.text ?? empty)
        self.dismiss(animated: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        Utility.setErrorTextField(isError: false, view: createResumeTxtFld, errorLabel: resumeErrorLbl)
        if (textField == createResumeTxtFld) {
            return true
        } else {
            return false
        }
    }
}
