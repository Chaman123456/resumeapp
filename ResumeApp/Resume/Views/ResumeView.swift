//
//  ViewController.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 12/02/23.
//

import UIKit

class ResumeView: UIViewController {
    @IBOutlet var resumeTableView: UITableView?
    @IBOutlet var noResumeView: UIView?
    var resumeViewModels = ResumeViewModel()
    @IBOutlet var noResumeLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUI()
    }
    
    func setupUI() {
        noResumeLabel?.text = NO_RESUME_TEXT
        resumeTableView?.contentInset = UIEdgeInsets(top: 10, left: 00, bottom: 65, right: 0);
        resumeViewModels.getResumes { isEmpty in
            if isEmpty {
                noResumeView?.isHidden = false
                resumeTableView?.isHidden = true
            } else {
                self.resumeTableView?.reloadData()
                noResumeView?.isHidden = true
                resumeTableView?.isHidden = false
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func addResumeAction() {
        self.view.endEditing(true)
        resumeViewModels.addResume(controller: self) {
            self.resumeTableView?.reloadData()
            self.noResumeView?.isHidden = true
            self.resumeTableView?.isHidden = false
        }
    }
}

extension ResumeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resumeViewModels.resumes?.count ?? zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResumeTableViewCell", for: indexPath as IndexPath) as! ResumeTableViewCell
        let resume = resumeViewModels.resumes?[indexPath.row] ?? Resume()
        cell.resumeTextLbl?.text = resume.resumeName
        cell.timetLbl?.text = "\(LAST_UPDATED) \(Utility.getFormattedDate(string: resume.createdAt ?? "\(Date())", formatter: "MMM d, yyyy"))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "ResumeDetailsView") as? ResumeDetailsView ?? ResumeDetailsView()
        home.resumeDetailsViewModel = ResumeDetailsViewModel()
        home.resumeDetailsViewModel?.resume = resumeViewModels.resumes?[indexPath.row] ?? Resume()
        home.completionHandler = { deleteResume in
            print("nothing")
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResumeTableViewCell", for: indexPath as IndexPath) as! ResumeTableViewCell
            cell.parent?.resumeTableView?.reloadData()
        }
        navigationController?.pushViewController(home, animated: true)
    }
}

class ResumeTableViewCell: UITableViewCell {
    @IBOutlet var resumeTextLbl: UILabel?
    @IBOutlet var timetLbl: UILabel?
    @IBOutlet var menuBtn: UIButton?
    var parent : ResumeView?
}

