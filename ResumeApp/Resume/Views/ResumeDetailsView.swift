//
//  ResumeDetailsView.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 20/08/23.
//

import UIKit

class ResumeDetailsView: UIViewController {
    @IBOutlet var resumeDetailsTblView: UITableView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var menuView: UIView?
    var resumeDetailsViewModel: ResumeDetailsViewModel?
    var tapGesture: UITapGestureRecognizer?
    var completionHandler: (String) -> Void = {_ in}
    var isHiddenMenu: Bool = false {
        didSet {
            if isHiddenMenu {
                menuView?.isHidden = false
                tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                self.view.addGestureRecognizer(tapGesture ?? UITapGestureRecognizer())
            } else {
                self.view.removeGestureRecognizer(tapGesture ??  UITapGestureRecognizer())
                menuView?.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        resumeDetailsTblView?.contentInset = UIEdgeInsets(top: 7, left: 00, bottom: 0, right: 0);
        titleLabel?.text = resumeDetailsViewModel?.resume?.resumeName ?? empty
        resumeDetailsViewModel?.getAllSection(completion: { check in
            self.resumeDetailsTblView?.reloadData()
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        isHiddenMenu = false
    }
    
    @IBAction func backAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func editAction() {
        isHiddenMenu = false
        self.view.endEditing(true)
        self.resumeDetailsViewModel?.editResumeName(viewController: self) {
            self.titleLabel?.text = self.resumeDetailsViewModel?.resume?.resumeName ?? empty
            self.view.makeToast(RESUME_NAME_UPDATED_SUCCESSFULLY)
        }
    }
    
    @IBAction func deleteAction() {
        isHiddenMenu = false
        self.view.endEditing(true)
        resumeDetailsViewModel?.deleteResume(viewController: self, completion: {
            self.completionHandler(empty)
        })
    }
    
    @IBAction func menuAction() {
        isHiddenMenu = !isHiddenMenu
    }
    
    @IBAction func duplicateAction() {
        isHiddenMenu = false
        self.view.endEditing(true)
        self.resumeDetailsViewModel?.addDuplicateResume(controller: self) {
             self.view.makeToast(DUPLICATE_RESUME_CREATED_SUCCESSFULLY)
        }
    }
}

extension ResumeDetailsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resumeDetailsViewModel?.resumeSectionArray?.count ?? zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResumeDetailsViewCell", for: indexPath as IndexPath) as? ResumeDetailsViewCell ?? ResumeDetailsViewCell()
        let resumeSection = resumeDetailsViewModel?.resumeSectionArray?[indexPath.row]
        switch resumeSection {
        case .personalDetails:
            let personalDetailsTable = PersonalDetailsTable()
            let personalDetails = personalDetailsTable.getPersonalDetailsFromTable(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = personalDetails.sectionName
        case .professionalSummary:
            let professionalSummaryTable = ProfessionalSummaryTable()
            let professionalDetails = professionalSummaryTable.getProfessionalSummary(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = professionalDetails.sectionName
        case .experienceSummary:
            let experienceSummaryTable = ExperienceSummaryTable()
            let experienceDetails = experienceSummaryTable.getExperienceSummary(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = experienceDetails.sectionName
        case .employer:
            let employerTable = EmployerTable()
            let experienceDetails = employerTable.getEmployerHistory(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = experienceDetails.sectionName
        case .education:
            let educationTable = EducationTable()
            let educationDetails = educationTable.getEducationHistory(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = educationDetails.sectionName
        case .websiteAndSocialLink:
            let websiteSocialLinkTable = WebsiteSocialLinkTable()
            let websiteSocialLinkDetails = websiteSocialLinkTable.getWebsiteSocialLink(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = websiteSocialLinkDetails.sectionName
        case .language:
            let languageTable = LanguageTable()
            let languages = languageTable.getLanguages(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = languages.sectionName
        case .reference:
            let referenceTable = ReferenceTable()
            let reference = referenceTable.getReference(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = reference.sectionName
        case .course:
            let courseTable = CourseTable()
            let course = courseTable.getCourses(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = course.sectionName
        case .project:
            let projectTable = ProjectTable()
            let project = projectTable.getProject(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = project.sectionName
        case .custom:
            let customTable = CustomTable()
            let custom = customTable.getCustom(resumeId: "\(resumeDetailsViewModel?.resume?.localId ?? zero)")
            cell.optionLbl?.text = custom.sectionName
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let resumeSection = resumeDetailsViewModel?.resumeSectionArray?[indexPath.row]
        switch resumeSection {
        case .personalDetails:
            let storyBoard: UIStoryboard = UIStoryboard(name: "PersonalDetails", bundle: nil)
            let personalDetails = storyBoard.instantiateViewController(withIdentifier: "PersonalDetailsView") as? PersonalDetailsView ?? PersonalDetailsView()
            personalDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            personalDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(personalDetails, animated: true)
        case .professionalSummary:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Professional", bundle: nil)
            let professionalDetails = storyBoard.instantiateViewController(withIdentifier: "ProfessionalSummaryView") as? ProfessionalSummaryView ?? ProfessionalSummaryView()
            professionalDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            professionalDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(professionalDetails, animated: true)
        case .experienceSummary:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Experience", bundle: nil)
            let experienceDetails = storyBoard.instantiateViewController(withIdentifier: "ExperienceView") as? ExperienceView ?? ExperienceView()
            experienceDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            experienceDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(experienceDetails, animated: true)
        case .employer:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Employment", bundle: nil)
            let employmentDetails = storyBoard.instantiateViewController(withIdentifier: "EmploymentView") as? EmploymentView ?? EmploymentView()
            employmentDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            employmentDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(employmentDetails, animated: true)
        case .education:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Education", bundle: nil)
            let educationDetails = storyBoard.instantiateViewController(withIdentifier: "EducationView") as? EducationView ?? EducationView()
            educationDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            educationDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(educationDetails, animated: true)
        case .websiteAndSocialLink:
            let storyBoard: UIStoryboard = UIStoryboard(name: "WebsiteLinks", bundle: nil)
            let WebsiteSocialLinksDetails = storyBoard.instantiateViewController(withIdentifier: "WebsiteSocialLinksView") as? WebsiteSocialLinksView ?? WebsiteSocialLinksView()
            WebsiteSocialLinksDetails.resume = resumeDetailsViewModel?.resume ?? Resume()
            WebsiteSocialLinksDetails.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(WebsiteSocialLinksDetails, animated: true)
        case .language:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Languages", bundle: nil)
            let language = storyBoard.instantiateViewController(withIdentifier: "LanguagesView") as? LanguagesView ?? LanguagesView()
            language.resume = resumeDetailsViewModel?.resume ?? Resume()
            language.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(language, animated: true)
        case .reference:
            let storyBoard: UIStoryboard = UIStoryboard(name: "References", bundle: nil)
            let references = storyBoard.instantiateViewController(withIdentifier: "ReferencesView") as? ReferencesView ?? ReferencesView()
            references.resume = resumeDetailsViewModel?.resume ?? Resume()
            references.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(references, animated: true)
        case .course:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Courses", bundle: nil)
            let courses = storyBoard.instantiateViewController(withIdentifier: "CoursesView") as? CoursesView ?? CoursesView()
            courses.resume = resumeDetailsViewModel?.resume ?? Resume()
            courses.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(courses, animated: true)
        case .project:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Project", bundle: nil)
            let project = storyBoard.instantiateViewController(withIdentifier: "ProjectView") as? ProjectView ?? ProjectView()
            project.resume = resumeDetailsViewModel?.resume ?? Resume()
            project.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(project, animated: true)
        case .custom:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Custom", bundle: nil)
            let custom = storyBoard.instantiateViewController(withIdentifier: "CustomView") as? CustomView ?? CustomView()
            custom.resume = resumeDetailsViewModel?.resume ?? Resume()
            custom.completionHandler = { text in
                self.resumeDetailsTblView?.reloadData()
            }
            self.navigationController?.pushViewController(custom, animated: true)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return tableView.isEditing
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedItem = resumeDetailsViewModel?.resumeSectionArray?.remove(at: sourceIndexPath.row)
        resumeDetailsViewModel?.resumeSectionArray?.insert(movedItem!, at: destinationIndexPath.row)
    }
}

class ResumeDetailsViewCell: UITableViewCell {
    @IBOutlet var optionLbl: UILabel?
}

