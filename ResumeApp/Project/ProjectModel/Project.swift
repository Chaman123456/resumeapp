//
//  Project.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import Foundation
import CoreData

class Project {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var technology: String?
    public var name: String?
    public var description: String?
    public var createdAt: String?
    public var updatedAt: String?
}
