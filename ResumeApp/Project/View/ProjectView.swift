//
//  ProjectView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class ProjectView: UIViewController {
    @IBOutlet var projectTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var projectTable = ProjectTable()
    var projectViewModel = ProjectViewModel()
    var selectedIndexPath = -1
    var projectArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    var myCell: ProjectCell?

    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func autofillFields() {
        projectViewModel.getProjectData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = self.projectViewModel.project?.sectionName
            }
        }
    }
    
    @IBAction func editProjectSectionName() {
        projectViewModel.editProjectSectionName(viewController: self) {
            self.projectViewModel.getProjectData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.projectViewModel.project?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreProjectAction() {
        projectArray.append(1)
        projectTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if projectArray.count > 0 {
            projectArray.remove(at: sender?.tag ?? zero)
        }
        self.projectTableView?.reloadData()
    }
}

extension ProjectView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectArray.count <= 0 ? 1 : projectArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let projectCell = tableView.dequeueReusableCell(withIdentifier: "ProjectCell", for: indexPath as IndexPath) as! ProjectCell
        projectCell.trashBtn?.tag = indexPath.row
        projectCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        if selectedIndexPath == indexPath.row {
            projectCell.expendableView?.isHidden = false
            projectCell.mainView?.borderWidth = 1
            projectCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            projectCell.expandBtn?.setImage(image, for: .normal)
            projectCell.trashBtn?.isHidden = false
            projectCell.technologyTxtFld?.becomeFirstResponder()
            projectCell.technologyTxtFld?.setLeftPaddingPoints(10)
            projectCell.technologyTxtFld?.setRightPaddingPoints(10)
            projectCell.nameTxtFld?.setLeftPaddingPoints(10)
            projectCell.nameTxtFld?.setRightPaddingPoints(10)
            projectCell.descriptionTxtFld?.setLeftPaddingPoints(10)
            projectCell.descriptionTxtFld?.setRightPaddingPoints(10)
            if indexPath.row == zero {
                projectCell.trashBtn?.isHidden = true
            } else {
                projectCell.trashBtn?.isHidden = false
            }
        } else {
            projectCell.expendableView?.isHidden = true
            projectCell.mainView?.borderWidth = 0
            projectCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            projectCell.expandBtn?.setImage(image, for: .normal)
            projectCell.trashBtn?.isHidden = true
        }
        return projectCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.projectTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 340
        } else {
            return 71
        }
    }
}

class ProjectCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var projectView : UIView?
    @IBOutlet var technologyTxtFld: UITextField?
    @IBOutlet var nameTxtFld: UITextField?
    @IBOutlet var descriptionTxtFld: UITextField?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: ProjectView?
}

