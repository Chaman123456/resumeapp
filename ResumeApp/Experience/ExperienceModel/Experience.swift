//
//  Experience.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 04/09/23.
//

import Foundation

class Experience {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var description: String?
    public var createdAt: String?
    public var updatedAt: String?
}
