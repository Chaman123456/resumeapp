//
//  ExperienceViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 04/09/23.
//

import Foundation
import CoreData
import UIKit

class ExperienceViewModel {
    let experienceSummaryTable = ExperienceSummaryTable()
    var experience: Experience?
    
    init() {
        self.experience = Experience()
    }
    
    func getExperienceSummaryData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.experience = experienceSummaryTable.getExperienceSummary(resumeId: "\(resumeId)")
        completion(self.experience?.localId ?? zero > zero ? true : false)
    }
    
    func editExperienceSummarySectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.errorText = ENTER_EXPERIENCE_SUMMARY
        vc.buttonTitle = UPDATE_TEXT
        vc.existingTextValue = experience?.sectionName ?? empty
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        experience?.sectionName = sectionName
        experience?.updatedAt = "\(Date())"
        experienceSummaryTable.updateExperienceSection(experience: experience ?? Experience())
    }
}



