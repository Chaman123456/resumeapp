//
//  ExperienceView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 04/09/23.
//

import UIKit

class ExperienceView: UIViewController, UITextViewDelegate {
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var summarytextView: UITextView?
    @IBOutlet var summaryView: UIView?
    @IBOutlet var summaryErrorLbl: UILabel?
    @IBOutlet var textCountLbl: UILabel?
    var completionHandler: (String) -> Void = {_ in}
    var resume: Resume?
    var experienceViewModel = ExperienceViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autoFillExperienceSummaryData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func autoFillExperienceSummaryData() {
        experienceViewModel.getExperienceSummaryData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = experienceViewModel.experience?.sectionName
                summarytextView?.text = experienceViewModel.experience?.description
                textCountLbl?.text = "\(summarytextView?.text.count ?? zero)/50+"
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        Utility.setErrorTextField(isError: false, view: summaryView, errorLabel: summaryErrorLbl)
        if (textView == summarytextView) {
            let charsInTextView = (summarytextView?.text.count ?? 0)
            let remainingChars = charsInTextView
            if (text == "\n") {
                let remainingChars = (summarytextView?.text.count ?? 0)
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            if (text != "\n"){
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            if textView.text.count > 50 {
                textCountLbl?.text = "\(remainingChars)/200"
            } else if textView.text.count < 50 {
                textCountLbl?.text = "\(remainingChars)/50+"
            }
            return true
        } else {
            return false
        }
    }
    
    func saveExperienceData() {
        self.view.endEditing(true)
        experienceViewModel.experience?.description = summarytextView?.text
        experienceViewModel.experience?.updatedAt = "\(Date())"
        experienceViewModel.experienceSummaryTable.updateExperienceSummary(experience: experienceViewModel.experience ?? Experience())
        let resumeTable = ResumeTable()
        resumeTable.updateUpdatedDate(updatedDate: "\(Date())", resumeId: resume?.localId ?? zero)
        self.navigationController?.popViewController(animated: true)
        SceneDelegate.getSceneDelegate().window?.makeToast(DATA_SAVED_TEXT)
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction() {
        if summarytextView?.text == empty {
            Utility.setErrorTextField(isError: true, view: summaryView, errorLabel: summaryErrorLbl, errorText: ENTER_EXPERIENCE_SUMMARY)
            return
        }
        saveExperienceData()
    }
    
    @IBAction func editAction() {
        experienceViewModel.editExperienceSummarySectionName(viewController: self) {
            self.experienceViewModel.getExperienceSummaryData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.experienceViewModel.experience?.sectionName
                }
            }
        }
    }
    
}
