//
//  WebsiteSocialLinksViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class WebsiteSocialLinksViewModel {
    var websiteSocialLinks: WebsiteSocialLinks?
    var websiteSocialLinkTable = WebsiteSocialLinkTable()
    
    init() {
        self.websiteSocialLinks = WebsiteSocialLinks()
    }
    
    func getWebsiteSocialLinksData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.websiteSocialLinks = websiteSocialLinkTable.getWebsiteSocialLink(resumeId: "\(resumeId)")
        completion(self.websiteSocialLinks?.localId ?? zero > zero ? true : false)
    }
    
    func editWebsiteSocialLinksSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_WEBSITE_NAME
        vc.errorText = PLEASE_ENTER_WEBSITE_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        websiteSocialLinks?.sectionName = sectionName
        websiteSocialLinks?.updatedAt = "\(Date())"
        websiteSocialLinkTable.updateEducationSection(websiteSocialLinks: websiteSocialLinks ?? WebsiteSocialLinks())
    }
}




