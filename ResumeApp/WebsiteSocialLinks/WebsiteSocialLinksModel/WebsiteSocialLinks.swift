//
//  WebsiteSocialLinks.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import Foundation
import CoreData

class WebsiteSocialLinks {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var websiteSocialLinkLabel: String?
    public var link: String?
    public var createdAt: String?
    public var updatedAt: String?
}
