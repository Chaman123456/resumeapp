//
//  WebsiteSocialLinksView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class WebsiteSocialLinksView: UIViewController {
    @IBOutlet var websiteSocialTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var websiteSocialLinksViewModel = WebsiteSocialLinksViewModel()
    var websiteSocialLinkTable = WebsiteSocialLinkTable()
    var selectedIndexPath = -1
    var linksArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func autofillFields() {
        websiteSocialLinksViewModel.getWebsiteSocialLinksData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = websiteSocialLinksViewModel.websiteSocialLinks?.sectionName
            }
        }
    }
    
    @IBAction func editSocialLinksSectionName() {
        websiteSocialLinksViewModel.editWebsiteSocialLinksSectionName(viewController: self) {
            self.websiteSocialLinksViewModel.getWebsiteSocialLinksData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.websiteSocialLinksViewModel.websiteSocialLinks?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreSocialLinksAction() {
        linksArray.append(1)
        websiteSocialTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if linksArray.count > 0 {
            linksArray.remove(at: sender?.tag ?? zero)
        }
        self.websiteSocialTableView?.reloadData()
    }
}

extension WebsiteSocialLinksView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return linksArray.count <= 0 ? 1 : linksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let linksCell = tableView.dequeueReusableCell(withIdentifier: "WebsiteSocialCell", for: indexPath as IndexPath) as! WebsiteSocialCell
        linksCell.trashBtn?.tag = indexPath.row
        linksCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        if selectedIndexPath == indexPath.row {
            linksCell.expendableView?.isHidden = false
            linksCell.mainView?.borderWidth = 1
            linksCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            linksCell.expandBtn?.setImage(image, for: .normal)
            linksCell.trashBtn?.isHidden = false
            linksCell.schoolTxtFld?.setLeftPaddingPoints(10)
            linksCell.schoolTxtFld?.setRightPaddingPoints(10)
            linksCell.websiteTxtFld?.setLeftPaddingPoints(10)
            linksCell.websiteTxtFld?.setRightPaddingPoints(10)
            linksCell.linkTxtFld?.setLeftPaddingPoints(10)
            linksCell.linkTxtFld?.setRightPaddingPoints(10)
            if indexPath.row == zero {
                linksCell.trashBtn?.isHidden = true
            } else {
                linksCell.trashBtn?.isHidden = false
            }
        } else {
            linksCell.expendableView?.isHidden = true
            linksCell.mainView?.borderWidth = 0
            linksCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            linksCell.expandBtn?.setImage(image, for: .normal)
            linksCell.trashBtn?.isHidden = true
        }
        return linksCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > -1 && indexPath.row == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.websiteSocialTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 280
        } else {
            return 71
        }
    }
}

class WebsiteSocialCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var linkView : UIView?
    @IBOutlet var schoolTxtFld: UITextField?
    @IBOutlet var websiteTxtFld: UITextField?
    @IBOutlet var linkTxtFld: UITextField?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    @IBOutlet var saveBtn : UIButton?
    @IBOutlet var cancelBtn : UIButton?
    var parentClass: WebsiteSocialLinksView?
}
