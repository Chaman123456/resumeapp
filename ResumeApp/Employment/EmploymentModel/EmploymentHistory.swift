//
//  EmploymentSummary.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 02/09/23.
//

import Foundation
import CoreData

class Employer {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var createdAt: String?
    public var updatedAt: String?
}

class EmployerDetail: Codable {
    public var employerId: String = empty
    public var jobTitle: String = empty
    public var employer: String = empty
    public var startDate: String = empty
    public var endDate: String = empty
    public var city: String = empty
    public var description: String = empty
}

