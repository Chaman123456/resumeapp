//
//  EmploymentView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 02/09/23.
//

import UIKit

class EmploymentView: UIViewController {
    @IBOutlet var employeTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var employmentSummaryViewModel = EmploymentSummaryViewModel()
    var employerTable = EmployerTable()
    var selectedIndexPath: Int?
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupUI() {
        employeTableView?.estimatedRowHeight = 0
        employeTableView?.estimatedSectionHeaderHeight = 0
        employeTableView?.estimatedSectionFooterHeight = 0
        employeTableView?.contentInset = UIEdgeInsets(top: 20, left: 00, bottom: 20, right: 0)
        autofillFields()
    }
    
    @objc func handleStartDatePicker(sender: UIDatePicker) {
        let indexPath = IndexPath(row: selectedIndexPath ?? 0, section: 0)
        let cell = self.employeTableView?.cellForRow(at: indexPath) as? EmploymentCell ?? EmploymentCell()
        let employerDetail = employmentSummaryViewModel.employerDetailArray[selectedIndexPath ?? 0]
        employerDetail.startDate = Utility.getFormattedDate(date: sender.date, formatter: "dd MMMM, yyyy")
        employmentSummaryViewModel.employerDetailArray.remove(at: selectedIndexPath ?? 0)
        employmentSummaryViewModel.employerDetailArray.insert(employerDetail, at: selectedIndexPath ?? 0)
        self.employeTableView?.reloadRows(at: [IndexPath(row: selectedIndexPath ?? 0, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    @objc func handleEndDatePicker(sender: UIDatePicker) {
        let indexPath = IndexPath(row: selectedIndexPath ?? 0, section: 0)
        let cell = self.employeTableView?.cellForRow(at: indexPath) as? EmploymentCell ?? EmploymentCell()
        let employerDetail = employmentSummaryViewModel.employerDetailArray[selectedIndexPath ?? 0]
        employerDetail.endDate = Utility.getFormattedDate(date: sender.date, formatter: "dd MMMM, yyyy")
        employmentSummaryViewModel.employerDetailArray.remove(at: selectedIndexPath ?? 0)
        employmentSummaryViewModel.employerDetailArray.insert(employerDetail, at: selectedIndexPath ?? 0)
        self.employeTableView?.reloadRows(at: [IndexPath(row: selectedIndexPath ?? 0, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func autofillFields() {
        employmentSummaryViewModel.getEmploymentHistoryData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = employmentSummaryViewModel.employer?.sectionName
                if employmentSummaryViewModel.employerDetailArray.isEmpty {
                    employmentSummaryViewModel.employerDetailArray.append(EmployerDetail())
                }
                self.employeTableView?.reloadData()
            }
        }
    }
    
    @IBAction func editEmploySectionName() {
        employmentSummaryViewModel.editEmploymentHistorySectionName(viewController: self) {
            self.employmentSummaryViewModel.getEmploymentHistoryData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.employmentSummaryViewModel.employer?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreEmploymentAction() {
        employmentSummaryViewModel.employerDetailArray.append(EmployerDetail())
        employeTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if !employmentSummaryViewModel.employerDetailArray.isEmpty {
            employmentSummaryViewModel.employerDetailArray.remove(at: sender?.tag ?? zero)
        }
        self.employeTableView?.reloadData()
    }
    
    @objc func saveAction(sender: UIButton?) {
        self.view.endEditing(true)
        saveDataFroCell()
        self.selectedIndexPath = nil
        self.employeTableView?.reloadData()
    }
    
    func saveDataFroCell() {
        let indexPath = IndexPath(row: selectedIndexPath ?? 0, section: 0)
        let cell = self.employeTableView?.cellForRow(at: indexPath) as? EmploymentCell ?? EmploymentCell()
        let employerDetail = employmentSummaryViewModel.employerDetailArray[selectedIndexPath ?? 0] 
        cell.titleLabel?.text = (cell.jobTitleTxtFld?.text ?? empty).isEmpty ? NOT_SPECIFIED : cell.jobTitleTxtFld?.text ?? empty
        employerDetail.jobTitle = cell.jobTitleTxtFld?.text ?? empty
        employerDetail.employer = cell.employerTxtFld?.text ?? empty
        employerDetail.startDate = cell.startDateTxtFld?.text ?? empty
        employerDetail.endDate = cell.endDateTxtFld?.text ?? empty
        employerDetail.city = cell.cityTxtFld?.text ?? empty
        employerDetail.description = cell.descriptionTxtView?.text ?? empty
        employmentSummaryViewModel.employerDetailArray.remove(at: selectedIndexPath ?? 0)
        employmentSummaryViewModel.employerDetailArray.insert(employerDetail, at: selectedIndexPath ?? 0)
        self.employeTableView?.reloadRows(at: [IndexPath(row: selectedIndexPath ?? 0, section: 0)], with: UITableView.RowAnimation.none)
    }
}

extension EmploymentView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employmentSummaryViewModel.employerDetailArray.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let employeCell = tableView.dequeueReusableCell(withIdentifier: "EmploymentCell", for: indexPath as IndexPath) as? EmploymentCell ?? EmploymentCell()
        
        let employerDetail = employmentSummaryViewModel.employerDetailArray[indexPath.row] 
        employeCell.titleLabel?.text = employerDetail.jobTitle.isEmpty ? NOT_SPECIFIED : employerDetail.jobTitle
        employeCell.jobTitleTxtFld?.text = employerDetail.jobTitle
        employeCell.employerTxtFld?.text = employerDetail.employer
        employeCell.startDateTxtFld?.text = employerDetail.startDate
        employeCell.endDateTxtFld?.text = employerDetail.endDate
        employeCell.cityTxtFld?.text = employerDetail.city
        employeCell.descriptionTxtView?.text = employerDetail.description
        
        employeCell.trashBtn?.tag = indexPath.row
        employeCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        employeCell.saveBtn?.tag = indexPath.row
        employeCell.saveBtn?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        employeCell.startDateTxtFld?.inputView = employeCell.startDatePicker
        employeCell.endDateTxtFld?.inputView = employeCell.endDatePicker
        employeCell.startDateTxtFld?.tag = indexPath.row
        employeCell.endDateTxtFld?.tag = indexPath.row
        employeCell.startDatePicker.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
        employeCell.endDatePicker.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        if #available(iOS 14, *) {
            employeCell.startDatePicker.preferredDatePickerStyle = .wheels
            employeCell.startDatePicker.backgroundColor = .white
            employeCell.endDatePicker.preferredDatePickerStyle = .wheels
            employeCell.endDatePicker.backgroundColor = .white
        }
        employeCell.jobTitleTxtFld?.setLeftPaddingPoints(10)
        employeCell.jobTitleTxtFld?.setRightPaddingPoints(10)
        employeCell.employerTxtFld?.setLeftPaddingPoints(10)
        employeCell.employerTxtFld?.setRightPaddingPoints(10)
        employeCell.startDateTxtFld?.setLeftPaddingPoints(10)
        employeCell.startDateTxtFld?.setRightPaddingPoints(10)
        employeCell.endDateTxtFld?.setLeftPaddingPoints(10)
        employeCell.endDateTxtFld?.setRightPaddingPoints(10)
        employeCell.cityTxtFld?.setLeftPaddingPoints(10)
        employeCell.cityTxtFld?.setRightPaddingPoints(10)
        employeCell.expandBtn?.transform = CGAffineTransform.identity
        if let index = selectedIndexPath, index == indexPath.row {
            employeCell.expendableView?.isHidden = false
            employeCell.mainView?.borderWidth = 1
            employeCell.mainView?.backgroundColor = nil
            UIView.animate(withDuration: 0.25) {
                employeCell.expandBtn?.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
            }
            employeCell.trashBtn?.isHidden = false
            if indexPath.row == zero {
                employeCell.trashBtn?.isHidden = true
            } else {
                employeCell.trashBtn?.isHidden = false
            }
        } else {
            employeCell.expendableView?.isHidden = true
            employeCell.mainView?.borderWidth = 0
            employeCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            UIView.animate(withDuration: 0.25) {
                employeCell.expandBtn?.transform = CGAffineTransform.identity
            }
            employeCell.trashBtn?.isHidden = true
        }
        return employeCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selectedIndexPath, index == indexPath.row {
            self.selectedIndexPath = nil
        } else {
            self.selectedIndexPath = indexPath.row
        }
        self.employeTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 875
        } else {
            return 71
        }
    }
}

extension EmploymentView: UITextViewDelegate, UITextFieldDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
      saveDataFroCell()
    }
}


class EmploymentCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var employerView : UIView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var jobTitleTxtFld: UITextField?
    @IBOutlet var employerTxtFld: UITextField?
    @IBOutlet var startDateTxtFld: UITextField?
    @IBOutlet var endDateTxtFld: UITextField?
    @IBOutlet var cityTxtFld: UITextField?
    @IBOutlet var descriptionTxtView: UITextView?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    @IBOutlet var saveBtn : UIButton?
    var parentClass: EmploymentView?
    var startDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
    var endDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone.current
        return datePicker
    }()
}
