//
//  EmploymentSummaryViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 02/09/23.
//

import CoreData
import UIKit

class EmploymentSummaryViewModel {
    let employerTable = EmployerTable()
    var employer: Employer?
    var employerDetailArray = [EmployerDetail]()
    
    init() {
        self.employer = Employer()
     }
    
    func getEmploymentHistoryData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.employer = employerTable.getEmployerHistory(resumeId: "\(resumeId)")
        completion(self.employer?.localId ?? zero > zero ? true : false)
    }
    
    func editEmploymentHistorySectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_EMPLOYMENT_HISTORY_NAME
        vc.errorText = PLEASE_ENTER_EMPLOYMENT_HISTORY_NAME
        vc.buttonTitle = UPDATE_TEXT
        vc.existingTextValue = employer?.sectionName ?? empty
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        employer?.sectionName = sectionName
        employer?.updatedAt = "\(Date())"
        employerTable.updateEmployerSection(employer: employer ?? Employer())
    }
}





