//
//  SettingsView.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 15/08/23.
//

import UIKit
import MessageUI

class SettingsView: UIViewController {
    @IBOutlet var profileOptionsTblView: UITableView?
    var settingsViewModel: SettingsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI() {
        settingsViewModel = SettingsViewModel(controller: self)
        profileOptionsTblView?.contentInset = UIEdgeInsets(top: 10, left: 00, bottom: 0, right: 0);
        settingsViewModel?.getSettings(completion: { isEmpty in
            self.profileOptionsTblView?.reloadData()
        })
    }
}

extension SettingsView: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            self.dismiss(animated: true, completion: nil)
        default:
            print("default")
        }
    }
}
 
extension SettingsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsViewModel?.settingsArray?.count ?? zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsViewCell", for: indexPath as IndexPath) as! SettingsViewCell
        let settingType = settingsViewModel?.settingsArray?[indexPath.row]
        cell.options?.text = settingType?.getTitle() ?? empty
        cell.optionsImg?.image = UIImage(named: settingType?.rawValue ?? empty)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            settingsViewModel?.sendMail()
        case 1:
            settingsViewModel?.policyLink = true
            settingsViewModel?.openPrivatePolicyView()
        case 4:
            settingsViewModel?.openPrivatePolicyView()
        case 5:
            settingsViewModel?.shareAction()
        default:
            break
        }
    }
}

class SettingsViewCell: UITableViewCell {
    @IBOutlet var optionsImg: UIImageView?
    @IBOutlet var options: UILabel?
}


