//
//  LinkView.swift
//  ResumeApp
//
//  Created by Anviam on 24/08/23.
//

import UIKit
import WebKit

class LinkView: UIViewController, WKUIDelegate {
    @IBOutlet var webView: WKWebView!
    var link = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myURL = URL(string:link)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
