//
//  SettingsViewModel.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 27/08/23.
//

import UIKit
import MessageUI

enum SettingType: String, CaseIterable {
    case feedback = "feedback"
    case policy = "policy"
    case rating = "rating"
    case premium = "premium"
    case termsOfUse = "termsofuse"
    case share = "share"
    
    func getTitle() -> String {
        switch self {
        case .feedback:
            return "Feedback"
        case .policy:
            return "Privacy Policy"
        case .rating:
            return "Rate App"
        case .premium:
            return "Go Premium"
        case .termsOfUse:
            return "Terms of use"
        case .share:
            return "Share"
        }
    }
}


class SettingsViewModel {
    var settingsArray: [SettingType]?
    var controller: UIViewController?
    var policyLink: Bool = false

    init(controller: UIViewController) {
        self.controller = controller
        self.settingsArray = [SettingType]()
    }
    
    func getSettings(completion: ((Bool) -> Void)) {
        self.settingsArray = [.feedback, .policy, .rating, .premium, .termsOfUse, .share]
        completion(self.settingsArray?.isEmpty ?? false ? true : false)
    }
    
    func sendMail() {
        if MFMailComposeViewController.canSendMail() {
            let message:String  = "Changes in mail composer ios 11"
            let composePicker = MFMailComposeViewController()
            composePicker.mailComposeDelegate = controller as? SettingsView ?? SettingsView()
            composePicker.delegate = controller as? SettingsView ?? SettingsView()
            composePicker.setToRecipients(["example@gmail.com"])
            composePicker.setSubject("Testing Email")
            composePicker.setMessageBody(message, isHTML: false)
            controller?.present(composePicker, animated: true, completion: nil)
        } else {
            self .showErrorMessage()
        }
    }
    
    func shareAction() {
        if let name = URL(string: "https://www.apple.com8"), !name.absoluteString.isEmpty {
          let objectsToShare = [name]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            controller?.present(activityVC, animated: true, completion: nil)
        } else {
        }
    }
    
    func openPrivatePolicyView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
        let linkView = storyBoard.instantiateViewController(withIdentifier: "LinkView") as? LinkView ?? LinkView()
        if policyLink {
            linkView.link = "https://www.apple.com"
            policyLink = false
        } else {
            linkView.link = "https://www.google.com"
        }
        controller?.navigationController?.pushViewController(linkView, animated: true)
    }
    
    func showErrorMessage() {
        let alertMessage = UIAlertController(title: "could not sent email", message: "check if your device have email support!", preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title:"Okay", style: UIAlertAction.Style.default, handler: nil)
        alertMessage.addAction(action)
        controller?.present(alertMessage, animated: true, completion: nil)
    }
}
