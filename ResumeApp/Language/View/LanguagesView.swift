//
//  LanguagesView.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import UIKit

class LanguagesView: UIViewController {
    @IBOutlet var languageTableView: UITableView?
    @IBOutlet var headingLbl: UILabel?
    @IBOutlet var addOneMoreBtn: UIButton?
    var resume: Resume?
    var languageViewModel = LanguageViewModel()
    var languageTable = LanguageTable()
    var selectedIndexPath = -1
    var languageArray = Array(repeating: 0, count: 1)
    var completionHandler: (String) -> Void = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autofillFields()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func autofillFields() {
        languageViewModel.getLanguagesData(resumeId: resume?.localId ?? zero) { success in
            if success {
                headingLbl?.text = self.languageViewModel.languages?.sectionName
            }
        }
    }
    
    @IBAction func editSocialLinksSectionName() {
        languageViewModel.editLanguagesSectionName(viewController: self) {
            self.languageViewModel.getLanguagesData(resumeId: self.resume?.localId ?? zero) { success in
                if success {
                    self.headingLbl?.text = self.languageViewModel.languages?.sectionName
                }
            }
        }
    }
    
    @IBAction func backAction() {
        completionHandler(empty)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreSocialLinksAction() {
        languageArray.append(1)
        languageTableView?.reloadData()
    }
    
    @objc func expandAction(sender: UIButton?) {
        if sender?.tag ?? -1 > -1 && sender?.tag == selectedIndexPath {
            self.selectedIndexPath = -1
        } else {
            self.selectedIndexPath = sender?.tag ?? -1
        }
        self.languageTableView?.reloadData()
    }
    
    @objc func trashAction(sender: UIButton?) {
        if languageArray.count > 0 {
            languageArray.remove(at: sender?.tag ?? zero)
        }
        self.languageTableView?.reloadData()
    }
}

extension LanguagesView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageArray.count <= 0 ? 1 : languageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let langCell = tableView.dequeueReusableCell(withIdentifier: "LanguagesCell", for: indexPath as IndexPath) as! LanguagesCell
        langCell.expandBtn?.tag = indexPath.row
        langCell.expandBtn?.addTarget(self, action: #selector(expandAction), for: .touchUpInside)
        langCell.trashBtn?.tag = indexPath.row
        langCell.trashBtn?.addTarget(self, action: #selector(trashAction), for: .touchUpInside)
        if selectedIndexPath == indexPath.row {
            langCell.expendableView?.isHidden = false
            langCell.mainView?.borderWidth = 1
            langCell.mainView?.backgroundColor = nil
            let image = UIImage(named: "collapseIcon")?.withRenderingMode(.alwaysTemplate)
            langCell.expandBtn?.setImage(image, for: .normal)
            langCell.trashBtn?.isHidden = false
            langCell.langTxtFld?.setLeftPaddingPoints(10)
            langCell.langTxtFld?.setRightPaddingPoints(10)
            langCell.levelTxtFld?.setLeftPaddingPoints(10)
            langCell.levelTxtFld?.setRightPaddingPoints(10)
            if indexPath.row == zero {
                langCell.trashBtn?.isHidden = true
            } else {
                langCell.trashBtn?.isHidden = false
            }
        } else {
            langCell.expendableView?.isHidden = true
            langCell.mainView?.borderWidth = 0
            langCell.mainView?.backgroundColor = UIColor("#F3F3F3")
            let image = UIImage(named: "expandIcon")?.withRenderingMode(.alwaysTemplate)
            langCell.expandBtn?.setImage(image, for: .normal)
            langCell.trashBtn?.isHidden = true
        }
        return langCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndexPath == indexPath.row {
            return 280
        } else {
            return 71
        }
    }
}

class LanguagesCell: UITableViewCell {
    @IBOutlet var mainView : UIView?
    @IBOutlet var expendableView : UIView?
    @IBOutlet var languageView : UIView?
    @IBOutlet var langTxtFld: UITextField?
    @IBOutlet var levelTxtFld: UITextField?
    @IBOutlet var expandBtn : UIButton?
    @IBOutlet var trashBtn : UIButton?
    var parentClass: LanguagesView?
}
