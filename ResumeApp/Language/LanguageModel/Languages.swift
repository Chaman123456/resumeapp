//
//  Languages.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import Foundation
import CoreData

class Languages {
    public var localId: Int?
    public var resumeId: String?
    public var sectionName: String?
    public var language: String?
    public var level: String?
    public var createdAt: String?
    public var updatedAt: String?
}
