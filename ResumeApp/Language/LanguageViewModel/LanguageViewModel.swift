//
//  LanguageViewModel.swift
//  ResumeApp
//
//  Created by Rajesh Kumar on 07/09/23.
//

import CoreData
import UIKit

class LanguageViewModel {
    var languages: Languages?
    var languageTable = LanguageTable()
    
    init() {
        self.languages = Languages()
    }
    
    func getLanguagesData(resumeId: Int, completion: ((Bool) -> Void)) {
        self.languages = languageTable.getLanguages(resumeId: "\(resumeId)")
        completion(self.languages?.localId ?? zero > zero ? true : false)
    }
    
    func editLanguagesSectionName(viewController: UIViewController, completion: @escaping (() -> Void)) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ResumeHelper", bundle:nil)
        let vc = (storyBoard.instantiateViewController(identifier: "CreateResumeAlert")) as! CreateResumeAlert
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3966007864)
        vc.placeHolderText = PLEASE_ENTER_LANGUAGE_NAME
        vc.errorText = PLEASE_ENTER_LANGUAGE_NAME
        vc.buttonTitle = SAVE_TEXT
        vc.completionHandler = { sectionName in
            self.saveToDB(sectionName: sectionName)
            completion()
        }
        viewController.present(vc, animated: false, completion: nil)
    }
    
    func saveToDB(sectionName: String) {
        languages?.sectionName = sectionName
        languages?.updatedAt = "\(Date())"
        languageTable.updateLanguageSection(languages: languages ?? Languages())
    }
}




