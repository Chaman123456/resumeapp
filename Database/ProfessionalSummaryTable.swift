//
//  ProfessionalSummary.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class ProfessionalSummaryTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createProfessionalSummaryTable() {
        let sqlLightTable = "CREATE TABLE ProfessionalSummaryTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', summary TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating ProfessionalSummaryTable")
        }
    }
    
    func saveProfessionalSummary(professionalSummary: ProfessionalSummary) -> Int {
        var professionalId = Int()
        statement = nil
        let query = "INSERT into ProfessionalSummaryTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, professionalSummary.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, professionalSummary.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, professionalSummary.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from ProfessionalSummaryTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                professionalId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return professionalId
    }
    
    func getProfessionalSummary(resumeId: String) -> ProfessionalSummary {
        let professionalSummary = ProfessionalSummary()
        let query = "Select * from ProfessionalSummaryTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                professionalSummary.localId = Int(sqlite3_column_int(statement, 0))
                professionalSummary.resumeId = String(cString: sqlite3_column_text(statement, 1))
                professionalSummary.sectionName = String(cString: sqlite3_column_text(statement, 2))
                professionalSummary.summary = String(cString: sqlite3_column_text(statement, 3))
                professionalSummary.createdAt = String(cString: sqlite3_column_text(statement, 4))
                professionalSummary.updatedAt = String(cString: sqlite3_column_text(statement, 5))
            }
            sqlite3_finalize(statement)
        }
        return professionalSummary
    }
    
    func updateProfessionalSection(professionalSummary: ProfessionalSummary) {
        statement = nil
        let query = "UPDATE ProfessionalSummaryTable set sectionName = ?, updatedAt = ? WHERE localId = '\(professionalSummary.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, professionalSummary.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, professionalSummary.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ProfessionalSummaryTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateProfessionalSummary(professionalSummary: ProfessionalSummary) {
        statement = nil
        let query = "UPDATE ProfessionalSummaryTable set summary = ?, updatedAt = ? WHERE localId = '\(professionalSummary.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, professionalSummary.summary, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, professionalSummary.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ProfessionalSummaryTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func deleteProfessionalSummary(resumeId: Int) {
        statement = nil
        let query = "DELETE FROM ProfessionalSummaryTable WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
        if sqlite3_step(statement) != SQLITE_DONE {
            print("Error in insert into ProfessionalSummaryTable")
        }
        }
        sqlite3_finalize(statement)
    }
}
