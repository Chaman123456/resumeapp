//
//  ProjectTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class ProjectTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createProjectTable() {
        let sqlLightTable = "CREATE TABLE ProjectTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', technology TEXT DEFAULT '', name TEXT DEFAULT '', description TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating ProjectTable")
        }
    }
    
    func saveProject(project: Project) -> Int {
        var projectId = Int()
        statement = nil
        let query = "INSERT into ProjectTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, project.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, project.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, project.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from ProjectTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                projectId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return projectId
    }
    
    func getProject(resumeId: String) -> Project {
        let project = Project()
        let query = "Select * from ProjectTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                project.localId = Int(sqlite3_column_int(statement, 0))
                project.resumeId = String(cString: sqlite3_column_text(statement, 1))
                project.sectionName = String(cString: sqlite3_column_text(statement, 2))
                project.technology = String(cString: sqlite3_column_text(statement, 3))
                project.name = String(cString: sqlite3_column_text(statement, 4))
                project.description = String(cString: sqlite3_column_text(statement, 5))
                project.createdAt = String(cString: sqlite3_column_text(statement, 6))
                project.updatedAt = String(cString: sqlite3_column_text(statement, 7))
            }
            sqlite3_finalize(statement)
        }
        return project
    }
    
    func updateProjectSection(project: Project) {
        statement = nil
        let query = "UPDATE ProjectTable set sectionName = ?, updatedAt = ? WHERE localId = '\(project.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, project.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, project.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into CourseTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateProject(project: Project) {
        statement = nil
        let query = "UPDATE ProjectTable set  technology = ?, name = ?, description = ?, updatedAt = ? WHERE localId = '\(project.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, project.technology, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, project.name, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, project.description, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, project.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ProjectTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
}

