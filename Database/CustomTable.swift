//
//  CustomTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class CustomTable: NSObject {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createCustomTable() {
        let sqlLightTable = "CREATE TABLE CustomTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', title TEXT DEFAULT '', startDate TEXT DEFAULT '', endDate TEXT DEFAULT '', city TEXT DEFAULT '', description TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating CustomTable")
        }
    }
    
    func saveCustom(custom: Custom) -> Int {
        var customId = Int()
        statement = nil
        let query = "INSERT into CustomTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, custom.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, custom.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, custom.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from CustomTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                customId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return customId
    }
    
    func getCustom(resumeId: String) -> Custom {
        let custom = Custom()
        let query = "Select * from CustomTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                custom.localId = Int(sqlite3_column_int(statement, 0))
                custom.resumeId = String(cString: sqlite3_column_text(statement, 1))
                custom.sectionName = String(cString: sqlite3_column_text(statement, 2))
                custom.title = String(cString: sqlite3_column_text(statement, 3))
                custom.startDate = String(cString: sqlite3_column_text(statement, 4))
                custom.endDate = String(cString: sqlite3_column_text(statement, 5))
                custom.city = String(cString: sqlite3_column_text(statement, 6))
                custom.description = String(cString: sqlite3_column_text(statement, 7))
                custom.createdAt = String(cString: sqlite3_column_text(statement, 8))
                custom.updatedAt = String(cString: sqlite3_column_text(statement, 9))
            }
            sqlite3_finalize(statement)
        }
        return custom
    }
    
    func updateCustomSection(custom: Custom) {
        statement = nil
        let query = "UPDATE CustomTable set sectionName = ?, updatedAt = ? WHERE localId = '\(custom.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, custom.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, custom.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into CustomTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateCustom(custom: Custom) {
        statement = nil
        let query = "UPDATE CustomTable set title = ?, startDate = ? , endDate = ?, city = ?, description = ?, updatedAt = ? WHERE localId = '\(custom.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, custom.title, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, custom.startDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, custom.endDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, custom.city, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, custom.description, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 6, custom.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into CustomTable")
            }
        }
        sqlite3_finalize(statement)
    }
}
