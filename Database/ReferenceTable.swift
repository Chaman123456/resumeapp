//
//  ReferenceTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class ReferenceTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createReferenceTable() {
        let sqlLightTable = "CREATE TABLE ReferenceTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', referenceFullName TEXT DEFAULT '', company TEXT DEFAULT '', phone TEXT DEFAULT '', email TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating ReferenceTable")
        }
    }
    
    func saveReference(references: References) -> Int {
        var referencesId = Int()
        statement = nil
        let query = "INSERT into ReferenceTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, references.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, references.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, references.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from ReferenceTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                referencesId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return referencesId
    }
    
    func getReference(resumeId: String) -> References {
        let references = References()
        let query = "Select * from ReferenceTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                references.localId = Int(sqlite3_column_int(statement, 0))
                references.resumeId = String(cString: sqlite3_column_text(statement, 1))
                references.sectionName = String(cString: sqlite3_column_text(statement, 2))
                references.referenceFullName = String(cString: sqlite3_column_text(statement, 3))
                references.company = String(cString: sqlite3_column_text(statement, 4))
                references.phone = String(cString: sqlite3_column_text(statement, 5))
                references.email = String(cString: sqlite3_column_text(statement, 6))
                references.createdAt = String(cString: sqlite3_column_text(statement, 7))
                references.updatedAt = String(cString: sqlite3_column_text(statement, 8))
            }
            sqlite3_finalize(statement)
        }
        return references
    }
    
    func updateReferenceSection(references: References) {
        statement = nil
        let query = "UPDATE ReferenceTable set sectionName = ?, updatedAt = ? WHERE localId = '\(references.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, references.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, references.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ReferenceTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateReference(references: References) {
        statement = nil
        let query = "UPDATE ReferenceTable set  referenceFullName = ?, company = ?, phone = ?, email = ?, updatedAt = ? WHERE localId = '\(references.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, references.referenceFullName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, references.company, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, references.phone, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, references.email, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, references.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ReferenceTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
}

