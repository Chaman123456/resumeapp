//
//  PersonalDetailsTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class PersonalDetailsTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createPersonalDetailsTable() {
        let sqlLightTable = "CREATE TABLE PersonalDetailsTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', jobTitle TEXT DEFAULT '', photo TEXT DEFAULT '', firstName TEXT DEFAULT '', lastName TEXT DEFAULT '', email TEXT DEFAULT '', phone TEXT DEFAULT '', country TEXT DEFAULT '', city TEXT DEFAULT '', address TEXT DEFAULT '', postalCode TEXT DEFAULT '', nationality TEXT DEFAULT '', dob TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating PersonalDetailsTable")
        }
    }
    
    func savePersonalDetails(personalDetails: PersonalDetails) -> Int {
        var personalId = Int()
        statement = nil
        let query = "INSERT into PersonalDetailsTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, personalDetails.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, personalDetails.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, personalDetails.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from PersonalDetailsTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                personalId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return personalId
    }
    
    func updateSection(personalDetails: PersonalDetails) {
        statement = nil
        let query = "UPDATE PersonalDetailsTable set sectionName = ?, updatedAt = ? WHERE localId = '\(personalDetails.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, personalDetails.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, personalDetails.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into PersonalDetailsTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updatePersonalDetails(personalDetails: PersonalDetails) {
        statement = nil
        let query = "UPDATE PersonalDetailsTable set jobTitle = ?, firstName = ?, lastName = ?, email = ?, phone = ?, country = ?, city = ?, address = ?, postalCode = ?, nationality = ?, dob = ?, updatedAt = ? WHERE localId = '\(personalDetails.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, personalDetails.jobTitle, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, personalDetails.firstName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, personalDetails.lastName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, personalDetails.email , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, personalDetails.phone , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 6, personalDetails.country , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 7, personalDetails.city , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 8, personalDetails.address , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 9, personalDetails.postalCode , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 10, personalDetails.nationality , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 11, personalDetails.dob , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 12, personalDetails.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into PersonalDetailsTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func getPersonalDetailsFromTable(resumeId: String) -> PersonalDetails {
        let personalDetails = PersonalDetails()
        let query = "Select * from PersonalDetailsTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                personalDetails.localId = Int(sqlite3_column_int(statement, 0))
                personalDetails.resumeId = String(cString: sqlite3_column_text(statement, 1))
                personalDetails.sectionName = String(cString: sqlite3_column_text(statement, 2))
                personalDetails.jobTitle = String(cString: sqlite3_column_text(statement, 3))
                personalDetails.photo = String(cString: sqlite3_column_text(statement, 4))
                personalDetails.firstName = String(cString: sqlite3_column_text(statement, 5))
                personalDetails.lastName = String(cString: sqlite3_column_text(statement, 6))
                personalDetails.email = String(cString: sqlite3_column_text(statement, 7))
                personalDetails.phone = String(cString: sqlite3_column_text(statement, 8))
                personalDetails.country = String(cString: sqlite3_column_text(statement, 9))
                personalDetails.city = String(cString: sqlite3_column_text(statement, 10))
                personalDetails.address = String(cString: sqlite3_column_text(statement, 11))
                personalDetails.postalCode = String(cString: sqlite3_column_text(statement, 12))
                personalDetails.nationality = String(cString: sqlite3_column_text(statement, 13))
                personalDetails.dob = String(cString: sqlite3_column_text(statement, 14))
                personalDetails.createdAt = String(cString: sqlite3_column_text(statement, 15))
                personalDetails.updatedAt = String(cString: sqlite3_column_text(statement, 16))
            }
            sqlite3_finalize(statement)
        }
        return personalDetails
    }
    
    func deletePersonalData(resumeId: Int) {
        statement = nil
        let query = "DELETE FROM PersonalDetailsTable WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
        if sqlite3_step(statement) != SQLITE_DONE {
            print("Error in insert into PersonalDetailsTable")
        }
        }
        sqlite3_finalize(statement)
    }
}
