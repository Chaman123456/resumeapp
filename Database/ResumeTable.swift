//
//  ResumeTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 16/08/23.
//

import UIKit
import SQLite3

class ResumeTable: Database {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createResumeTable() {
        let sqlLightTable = "CREATE TABLE ResumeTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeName TEXT, createdAt TEXT, updatedAt TEXT DEFAULT '', personalDetails TEXT DEFAULT '', professionalSummary TEXT DEFAULT '', experienceSummary TEXT DEFAULT '', employer TEXT DEFAULT '', education TEXT DEFAULT '', websiteAndSocialLink TEXT DEFAULT '', project TEXT DEFAULT '', skill TEXT DEFAULT '', language TEXT DEFAULT '', reference TEXT DEFAULT '', course TEXT DEFAULT '', custom TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating ResumeTable")
        }
    }
    
    func saveResumeName(resume: Resume) -> Int {
        var resumeId = 0
        statement = nil
        let query = "INSERT into ResumeTable(resumeName, createdAt) VALUES (?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, resume.resumeName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, resume.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
            sqlite3_finalize(statement)
            statement = nil
            let query = "SELECT localId from ResumeTable ORDER BY localId DESC LIMIT 1"
            if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
                if sqlite3_step(statement) != SQLITE_DONE {
                    resumeId = Int(sqlite3_column_int(statement, 0))
                }
            }
        }
        sqlite3_finalize(statement);
        return resumeId
    }
    
    func getResumeFromTable() -> [Resume] {
        var resultArray = [Resume]()
        statement = nil
        let query = "Select * from ResumeTable"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let resume = Resume()
                resume.localId = Int(sqlite3_column_int(statement, 0))
                resume.resumeName = String(cString: sqlite3_column_text(statement, 1))
                resume.createdAt = String(cString: sqlite3_column_text(statement, 2))
                resume.updatedAt = String(cString: sqlite3_column_text(statement, 3))
                resume.personalDetails = String(cString: sqlite3_column_text(statement, 4))
                resume.professionalSummary = String(cString: sqlite3_column_text(statement, 5))
                resume.experienceSummary = String(cString: sqlite3_column_text(statement, 6))
                resume.employer = String(cString: sqlite3_column_text(statement, 7))
                resume.education = String(cString: sqlite3_column_text(statement, 8))
                resume.websiteAndSocialLink = String(cString: sqlite3_column_text(statement, 9))
                resume.project = String(cString: sqlite3_column_text(statement, 10))
                resume.skill = String(cString: sqlite3_column_text(statement, 11))
                resume.language = String(cString: sqlite3_column_text(statement, 12))
                resume.reference = String(cString: sqlite3_column_text(statement, 13))
                resume.course = String(cString: sqlite3_column_text(statement, 14))
                resume.custom = String(cString: sqlite3_column_text(statement, 15))
                resultArray.append(resume)
            }
            sqlite3_finalize(statement)
        }
        return resultArray
    }
    
    func updateDetail(personalId: String, professionalId: String, employerId: String, resumeId: Int, experienceId: String, educationId: String, websiteAndSocialLinkId: String, languageId: String, referencesId: String, courseId: String, projectId: String, skillId: String, customId: String) {
        statement = nil
        let query = "UPDATE ResumeTable set personalDetails = ?, professionalSummary = ?, employer = ?, experienceSummary = ?, education = ?, websiteAndSocialLink = ?, language = ?, reference = ?, course = ?, project = ?, skill = ?, custom = ? WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, personalId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, professionalId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, employerId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, experienceId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, educationId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 6, websiteAndSocialLinkId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 7, languageId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 8, referencesId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 9, courseId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 10, projectId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 11, skillId , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 12, customId , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ResumeTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateUpdatedDate(updatedDate: String, resumeId: Int) {
        statement = nil
        let query = "UPDATE ResumeTable set updatedAt = ? WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, updatedDate , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in insert into ResumeTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func deleteResumeData(resumeId: Int) -> Bool {
        statement = nil
        let query = "DELETE FROM ResumeTable WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
        if sqlite3_step(statement) != SQLITE_DONE {
            print("Error in insert into ResumeTable")
        }
        }
        sqlite3_finalize(statement)
        return true
    }
    
    func updateResumeName(resumeName: String, resumeId: Int) {
        statement = nil
        let query = "UPDATE ResumeTable set resumeName = ? WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, resumeName , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update resume name into ResumeTable")
            }
        }
        sqlite3_finalize(statement)
    }
}
