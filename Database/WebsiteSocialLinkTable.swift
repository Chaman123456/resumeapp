//
//  WebsiteSocialLinkTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class WebsiteSocialLinkTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createWebsiteSocialLinkTable() {
        let sqlLightTable = "CREATE TABLE WebsiteSocialLinkTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', WebsiteSocialLinkLabel TEXT DEFAULT '', link TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating WebsiteSocialLinkTable")
        }
    }
    
    func saveWebsiteSocialLinkTable(websiteSocialLinks: WebsiteSocialLinks) -> Int {
        var linksId = Int()
        statement = nil
        let query = "INSERT into WebsiteSocialLinkTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, websiteSocialLinks.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, websiteSocialLinks.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, websiteSocialLinks.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from WebsiteSocialLinkTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                linksId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return linksId
    }
    
    func getWebsiteSocialLink(resumeId: String) -> WebsiteSocialLinks {
        let links = WebsiteSocialLinks()
        let query = "Select * from WebsiteSocialLinkTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                links.localId = Int(sqlite3_column_int(statement, 0))
                links.resumeId = String(cString: sqlite3_column_text(statement, 1))
                links.sectionName = String(cString: sqlite3_column_text(statement, 2))
                links.websiteSocialLinkLabel = String(cString: sqlite3_column_text(statement, 3))
                links.link = String(cString: sqlite3_column_text(statement, 4))
                links.createdAt = String(cString: sqlite3_column_text(statement, 5))
                links.updatedAt = String(cString: sqlite3_column_text(statement, 6))
            }
            sqlite3_finalize(statement)
        }
        return links
    }
    
    func updateEducationSection(websiteSocialLinks: WebsiteSocialLinks) {
        statement = nil
        let query = "UPDATE WebsiteSocialLinkTable set sectionName = ?, updatedAt = ? WHERE localId = '\(websiteSocialLinks.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, websiteSocialLinks.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, websiteSocialLinks.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into WebsiteSocialLinkTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateEducation(websiteSocialLinks: WebsiteSocialLinks) {
        statement = nil
        let query = "UPDATE WebsiteSocialLinkTable set set websiteSocialLinkLabel = ?, updatedAt = ? WHERE localId = '\(websiteSocialLinks.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, websiteSocialLinks.websiteSocialLinkLabel, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, websiteSocialLinks.websiteSocialLinkLabel, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, websiteSocialLinks.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into WebsiteSocialLinkTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
}
