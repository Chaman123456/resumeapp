//
//  LanguageTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class LanguageTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createLanguageTable() {
        let sqlLightTable = "CREATE TABLE LanguageTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', language TEXT DEFAULT '', level TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating LanguageTable")
        }
    }
    
    func saveLanguages(languages: Languages) -> Int {
        var linksId = Int()
        statement = nil
        let query = "INSERT into LanguageTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, languages.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, languages.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, languages.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from LanguageTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                linksId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return linksId
    }
    
    func getLanguages(resumeId: String) -> Languages {
        let language = Languages()
        let query = "Select * from LanguageTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                language.localId = Int(sqlite3_column_int(statement, 0))
                language.resumeId = String(cString: sqlite3_column_text(statement, 1))
                language.sectionName = String(cString: sqlite3_column_text(statement, 2))
                language.language = String(cString: sqlite3_column_text(statement, 3))
                language.level = String(cString: sqlite3_column_text(statement, 4))
                language.createdAt = String(cString: sqlite3_column_text(statement, 5))
                language.updatedAt = String(cString: sqlite3_column_text(statement, 6))
            }
            sqlite3_finalize(statement)
        }
        return language
    }
    
    func updateLanguageSection(languages: Languages) {
        statement = nil
        let query = "UPDATE LanguageTable set sectionName = ?, updatedAt = ? WHERE localId = '\(languages.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, languages.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, languages.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into LanguageTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateLanguage(languages: Languages) {
        statement = nil
        let query = "UPDATE LanguageTable set set language = ?, level = ? updatedAt = ? WHERE localId = '\(languages.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, languages.language, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, languages.level, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, languages.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into LanguageTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
}
