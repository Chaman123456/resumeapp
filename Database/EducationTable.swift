//
//  EducationTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class EducationTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createEducationTable() {
        let sqlLightTable = "CREATE TABLE EducationTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', school TEXT DEFAULT '', degree TEXT DEFAULT '', startDate TEXT DEFAULT '', endDate TEXT DEFAULT '', city TEXT DEFAULT '', description TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating EducationTable")
        }
    }
    
    func saveEducationHistory(education: Education) -> Int {
        var educationId = Int()
        statement = nil
        let query = "INSERT into EducationTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, education.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, education.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, education.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from EducationTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                educationId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return educationId
    }
    
    func getEducationHistory(resumeId: String) -> Education {
        let education = Education()
        let query = "Select * from EducationTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                education.localId = Int(sqlite3_column_int(statement, 0))
                education.resumeId = String(cString: sqlite3_column_text(statement, 1))
                education.sectionName = String(cString: sqlite3_column_text(statement, 2))
                education.school = String(cString: sqlite3_column_text(statement, 3))
                education.degree = String(cString: sqlite3_column_text(statement, 4))
                education.startDate = String(cString: sqlite3_column_text(statement, 5))
                education.endDate = String(cString: sqlite3_column_text(statement, 6))
                education.city = String(cString: sqlite3_column_text(statement, 7))
                education.description = String(cString: sqlite3_column_text(statement, 8))
                education.createdAt = String(cString: sqlite3_column_text(statement, 9))
                education.updatedAt = String(cString: sqlite3_column_text(statement, 10))
            }
            sqlite3_finalize(statement)
        }
        return education
    }
    
    func updateEducationSection(education: Education) {
        statement = nil
        let query = "UPDATE EducationTable set sectionName = ?, updatedAt = ? WHERE localId = '\(education.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, education.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, education.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into EducationTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateEducation(education: Education) {
        statement = nil
        let query = "UPDATE EducationTable set set school = ?, degree = ? , startDate = ?, endDate = ?, city = ?, description = ?, updatedAt = ? WHERE localId = '\(education.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, education.school, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, education.degree, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, education.startDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, education.endDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, education.city, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 6, education.description, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 7, education.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into EducationTable")
            }
        }
        sqlite3_finalize(statement)
    }

}
