//
//  SkillTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class SkillTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createSkillTable() {
        let sqlLightTable = "CREATE TABLE SkillTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeName TEXT, createdAt TEXT, updatedAt TEXT DEFAULT '', personalDetails TEXT DEFAULT '', professionalSummary TEXT DEFAULT '', experienceSummary TEXT DEFAULT '', employer TEXT DEFAULT '', education TEXT DEFAULT '', websiteAndSocialLink TEXT DEFAULT '', project TEXT DEFAULT '', skill TEXT DEFAULT '', language TEXT DEFAULT '', reference TEXT DEFAULT '', cource TEXT DEFAULT '', custom TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating SkillTable")
        }
    }

}
