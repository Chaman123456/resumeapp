//
//  CoverLetter.swift
//  ResumeApp
//
//  Created by Anviam on 19/08/23.
//

import UIKit
import SQLite3

class CoverLetterTable: Database {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createCoverLetterTable() {
        let sqlLightTable = "CREATE TABLE CoverLetterTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, coverletterName TEXT, createdAt TEXT, updatedAt TEXT)"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating CoverLetterTable")
        }
    }
    func saveCoverLetter(coverLetter: CoverLetterModel) {
        statement = nil
        let query = "INSERT into CoverLetterTable(localId, coverletterName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 2, coverLetter.name , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, coverLetter.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement);
    }
    
    func getCoverLetterFromTable() -> [CoverLetterModel] {
        var resultArray = [CoverLetterModel]()
        statement = nil
        let query = "Select * from CoverLetterTable"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let coverLetter = CoverLetterModel()
                coverLetter.name = String(cString: sqlite3_column_text(statement, 1))
                coverLetter.createdAt = String(cString: sqlite3_column_text(statement, 2))
                resultArray.append(coverLetter)
            }
            sqlite3_finalize(statement)
        }
        return resultArray
    }
}


