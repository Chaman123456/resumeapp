//
//  CourseTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class CourseTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createCoursesTable() {
        let sqlLightTable = "CREATE TABLE CourseTable(localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', course TEXT DEFAULT '', institution TEXT DEFAULT '', startDate TEXT DEFAULT '', endDate TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating CourseTable")
        }
    }
    
    func saveCourses(courses: Courses) -> Int {
        var coursesId = Int()
        statement = nil
        let query = "INSERT into CourseTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, courses.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, courses.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, courses.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from CourseTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                coursesId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return coursesId
    }
    
    func getCourses(resumeId: String) -> Courses {
        let courses = Courses()
        let query = "Select * from CourseTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                courses.localId = Int(sqlite3_column_int(statement, 0))
                courses.resumeId = String(cString: sqlite3_column_text(statement, 1))
                courses.sectionName = String(cString: sqlite3_column_text(statement, 2))
                courses.course = String(cString: sqlite3_column_text(statement, 3))
                courses.instiution = String(cString: sqlite3_column_text(statement, 4))
                courses.startDate = String(cString: sqlite3_column_text(statement, 5))
                courses.endDate = String(cString: sqlite3_column_text(statement, 6))
                courses.createdAt = String(cString: sqlite3_column_text(statement, 7))
                courses.updatedAt = String(cString: sqlite3_column_text(statement, 8))
            }
            sqlite3_finalize(statement)
        }
        return courses
    }
    
    func updateCoursesSection(courses: Courses) {
        statement = nil
        let query = "UPDATE CourseTable set sectionName = ?, updatedAt = ? WHERE localId = '\(courses.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, courses.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, courses.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into CourseTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateCourses(courses: Courses) {
        statement = nil
        let query = "UPDATE CourseTable set  course = ?, instiution = ?, startDate = ?, endDate = ?, updatedAt = ? WHERE localId = '\(courses.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, courses.course, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, courses.instiution, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, courses.startDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 4, courses.endDate, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 5, courses.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into CourseTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
}

