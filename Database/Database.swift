//
//  Database.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 16/08/23.
//

import UIKit
import SQLite3

class Database: NSObject {
    static var databaseConnection: OpaquePointer? = nil
    
    func printErroMessage() -> String {
        return String(cString:sqlite3_errmsg(Database.databaseConnection))
    }
    
    func getStringAt(statement:OpaquePointer, column:Int ) -> String? {
        let cColumn:CInt = CInt(column)
        let c = sqlite3_column_text(statement, cColumn)
        if ( c != nil ) {
            let cStringPtr = UnsafePointer<UInt8>(c)
            return String(cString:cStringPtr!)
        } else  {
            return empty
        }
    }
    
    func getIntAt(statement:OpaquePointer, column:Int) -> Int {
        let cColumn:CInt = CInt(column)
        return Int(sqlite3_column_int(statement, cColumn))
    }
    
    class func createDatabase() {
        print(sqlite3_libversion()!)
        print(sqlite3_threadsafe())
        openDatabase()
        let resumeTable = ResumeTable()
        resumeTable.createResumeTable()
        let coverLetterTable = CoverLetterTable()
        coverLetterTable.createCoverLetterTable()
        let personalDetailsTable = PersonalDetailsTable()
        personalDetailsTable.createPersonalDetailsTable()
        let professionalSummaryTable = ProfessionalSummaryTable()
        professionalSummaryTable.createProfessionalSummaryTable()
        let experienceSummaryTable = ExperienceSummaryTable()
        experienceSummaryTable.createExperienceSummaryTable()
        let employerTable = EmployerTable()
        employerTable.createEmployerTable()
        let educationTable = EducationTable()
        educationTable.createEducationTable()
        let websiteSocialLinkTable = WebsiteSocialLinkTable()
        websiteSocialLinkTable.createWebsiteSocialLinkTable()
        let projectTable = ProjectTable()
        projectTable.createProjectTable()
        let skillTable = SkillTable()
        skillTable.createSkillTable()
        let languageTable = LanguageTable()
        languageTable.createLanguageTable()
        let referenceTable = ReferenceTable()
        referenceTable.createReferenceTable()
        let courseTable = CourseTable()
        courseTable.createCoursesTable()
        let customTable = CustomTable()
        customTable.createCustomTable()
    }
    
    class func openDatabase() {
        if sqlite3_open_v2(getDBPath(), &databaseConnection, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) == SQLITE_OK {
            print("Successfully opened connection to database")
        } else {
            print("Unable to open database.")
        }
    }
    
    class func getDBPath() -> String {
        let paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDir: String? = (paths[0] as? String)
        let folderDir: String = documentsDir! + "/ResumeApp.db"
        print(folderDir)
        return folderDir
    }
}

extension Database {
    static func deleteTable(tableName: String) {
        let querySQL = "delete from \(tableName)"
        var localStatement: OpaquePointer? = nil
        sqlite3_prepare_v2(Database.databaseConnection, querySQL, -1, &localStatement, nil)
        sqlite3_step(localStatement)
        sqlite3_reset(localStatement)
    }
    
    static func alterTable(tableName: String, dictArray : [[String : String]]) {
        for(_, dict) in dictArray.enumerated() {
            let querySQL = "ALTER TABLE \(tableName) ADD \(dict["column"]!) TEXT default '\(dict["defaultValue"]!)'"
            var localStatement: OpaquePointer? = nil
            sqlite3_prepare_v2(Database.databaseConnection, querySQL, -1, &localStatement, nil)
            sqlite3_step(localStatement)
            sqlite3_reset(localStatement)
        }
    }
}

