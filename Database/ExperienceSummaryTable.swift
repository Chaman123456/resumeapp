//
//  ExperienceSummary.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class ExperienceSummaryTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createExperienceSummaryTable() {
        let sqlLightTable = "CREATE TABLE ExperienceSummaryTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', description TEXT DEFAULT '',createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating ExperienceSummaryTabl")
        }
    }
    
    func saveExperienceSummary(experience: Experience) -> Int {
        var experienceId = Int()
        statement = nil
        let query = "INSERT into ExperienceSummaryTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, experience.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, experience.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, experience.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from ExperienceSummaryTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                experienceId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return experienceId
    }
    
    func getExperienceSummary(resumeId: String) -> Experience {
        let experience = Experience()
        let query = "Select * from ExperienceSummaryTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                experience.localId = Int(sqlite3_column_int(statement, 0))
                experience.resumeId = String(cString: sqlite3_column_text(statement, 1))
                experience.sectionName = String(cString: sqlite3_column_text(statement, 2))
                experience.description = String(cString: sqlite3_column_text(statement, 3))
                experience.createdAt = String(cString: sqlite3_column_text(statement, 4))
                experience.updatedAt = String(cString: sqlite3_column_text(statement, 5))
            }
            sqlite3_finalize(statement)
        }
        return experience
    }
    
    func updateExperienceSection(experience: Experience) {
        statement = nil
        let query = "UPDATE ExperienceSummaryTable set sectionName = ?, updatedAt = ? WHERE localId = '\(experience.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, experience.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, experience.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ExperienceSummaryTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func updateExperienceSummary(experience: Experience) {
        statement = nil
        let query = "UPDATE ExperienceSummaryTable set description = ?, updatedAt = ? WHERE localId = '\(experience.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, experience.description, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, experience.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into ExperienceSummaryTable")
            }
        }
        sqlite3_finalize(statement)
    }
    
    func deleteProfessionalSummary(resumeId: Int) {
        statement = nil
        let query = "DELETE FROM ProfessionalSummaryTable WHERE localId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
        if sqlite3_step(statement) != SQLITE_DONE {
            print("Error in insert into ProfessionalSummaryTable")
        }
        }
        sqlite3_finalize(statement)
    }
}

