//
//  EmployerTable.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 22/08/23.
//

import UIKit
import SQLite3

class EmployerTable {
    let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    var statement: OpaquePointer? = nil
    
    func createEmployerTable() {
        let sqlLightTable = "CREATE TABLE EmployerTable (localId INTEGER PRIMARY KEY AUTOINCREMENT, resumeId TEXT, sectionName TEXT DEFAULT '', createdAt TEXT, updatedAt TEXT DEFAULT '')"
        if sqlite3_exec(Database.databaseConnection, sqlLightTable, nil, nil, nil) != SQLITE_OK {
            print("Error in creating EmployerTable")
        }
    }
    
    func saveEmploymentHistory(employer: Employer) -> Int {
        var employerId = Int()
        statement = nil
        let query = "INSERT into EmployerTable(resumeId, sectionName, createdAt) VALUES (?, ?, ?)"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, employer.resumeId, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, employer.sectionName , -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 3, employer.createdAt , -1, SQLITE_TRANSIENT)
            if SQLITE_DONE != sqlite3_step(statement) {
                print(sqlite3_errmsg(statement)!)
            }
        }
        sqlite3_finalize(statement)
        statement = nil;
        let selectQuery = "SELECT localId from EmployerTable ORDER BY localId DESC LIMIT 1";
        if sqlite3_prepare_v2(Database.databaseConnection, selectQuery, -1, &statement, nil) == SQLITE_OK {
            if (sqlite3_step(statement) != SQLITE_DONE) {
                employerId = Int(sqlite3_column_int(statement, 0))
            }
        }
        sqlite3_finalize(statement)
        return employerId
    }
    
    func getEmployerHistory(resumeId: String) -> Employer {
        let employer = Employer()
        let query = "Select * from EmployerTable WHERE resumeId = '\(resumeId)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                employer.localId = Int(sqlite3_column_int(statement, 0))
                employer.resumeId = String(cString: sqlite3_column_text(statement, 1))
                employer.sectionName = String(cString: sqlite3_column_text(statement, 2))
                employer.createdAt = String(cString: sqlite3_column_text(statement, 3))
                employer.updatedAt = String(cString: sqlite3_column_text(statement, 4))
            }
            sqlite3_finalize(statement)
        }
        return employer
    }
    
    func updateEmployerSection(employer: Employer) {
        statement = nil
        let query = "UPDATE EmployerTable set sectionName = ?, updatedAt = ? WHERE localId = '\(employer.localId ?? zero)'"
        if sqlite3_prepare_v2(Database.databaseConnection, query, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 1, employer.sectionName, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(statement, 2, employer.updatedAt , -1, SQLITE_TRANSIENT)
            if sqlite3_step(statement) != SQLITE_DONE {
                print("Error in update into EmployerTable")
            }
        }
        sqlite3_finalize(statement)
    } 
}
 
