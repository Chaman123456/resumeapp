//
//  StringConstant.swift
//  ResumeApp
//
//  Created by Chaman Sharma on 16/08/23.
//

import UIKit

var ENTER_RESUME_NAME: String {
    get {
        return "Enter Resume Name"
    }
}

var VALID_RESUME_NAME: String {
    get {
        return "Please enter valid resume name"
    }
}

var PLEASE_ENTER_RESUME_NAME: String {
    get {
        return "Please enter resume name"
    }
}

var PLEASE_ENTER_CUSTOM_NAME: String {
    get {
        return "Please enter custom name"
    }
}

var PLEASE_ENTER_COURSE_NAME: String {
    get {
        return "Please enter course name"
    }
}

var PLEASE_ENTER_REFERENCE_NAME: String {
    get {
        return "Please enter reference name"
    }
}

var PLEASE_ENTER_WEBSITE_NAME: String {
    get {
        return "Please enter website name"
    }
}

var PLEASE_ENTER_EMPLOYMENT_HISTORY_NAME: String {
    get {
        return "Please enter employment name"
    }
}

var PLEASE_ENTER_LANGUAGE_NAME: String {
    get {
        return "Please enter language name"
    }
}

var PLEASE_ENTER_PROJECT_NAME: String {
    get {
        return "Please enter project name"
    }
}

var ENTER_COVER_LETTER_NAME: String {
    get {
        return "Enter Cover Letter Name"
    }
}

var ENTER_SUMMARY: String {
    get {
        return "Please enter professional summary"
    }
}

var ENTER_EXPERIENCE_SUMMARY: String {
    get {
        return "Please enter experience summary"
    }
}

var ENTER_EDUCATION_SUMMARY: String {
    get {
        return "Please enter education name"
    }
}

var ENTER_SECTION_TITLE: String {
    get {
        return "Enter section title"
    }
}

var PLEASE_ENTER_SECTION_TITLE: String {
    get {
        return "Please enter section title"
    }
}


var LAST_UPDATED: String {
    get {
        return "Updated on"
    }
}

var NO_RESUME_TEXT: String {
    get {
        return "There isn't a resume yet, so kindly click the '+' button to create one."
    }
}

var PLEASE_ENTER_JOBTITLE: String {
    get {
        return "Please enter job title."
    }
}

var PLEASE_ENTER_FIRSTNAME: String {
    get {
        return "Please enter your first name."
    }
}

var PLEASE_ENTER_EMAIL: String {
    get {
        return "Please enter your email."
    }
}

var PLEASE_ENTER_VALID_EMAIL: String {
    get {
        return "Please enter a valid email address."
    }
}

var PLEASE_ENTER_PHONENUMBER: String {
    get {
        return "Please enter your Phone number."
    }
}

var PLEASE_ENTER_VALID_PHONE: String {
    get {
        return "Please enter a valid phone number."
    }
}

var PLEASE_ENTER_COUNTRY: String {
    get {
        return "Please enter your country."
    }
}

var PLEASE_ENTER_CITY: String {
    get {
        return "Please enter your city."
    }
}

var DATA_SAVED_TEXT: String {
    get {
        return "Data has been saved successfully"
    }
}

var SOMETHING_WENT_WRONG: String {
    get {
        return "Something went wrong"
    }
}

var ADD_ONE_MORE_EDUCATION: String {
    get {
        return "+ Add one more education"
    }
}

var ADD_EDUCATION: String {
    get {
        return "+ Add education"
    }
}

var ADD_ONE_MORE_EMPLOYMENT: String {
    get {
        return "+ Add one more employment"
    }
}

var ADD_EMPLOYMENT: String {
    get {
        return "+ Add employment"
    }
}

var ADD_ONE_MORE_LINK: String {
    get {
        return "+ Add one more link"
    }
}

var ADD_LINK: String {
    get {
        return "+ Add link"
    }
}

var ADD_ONE_MORE_SKILL: String {
    get {
        return "+ Add one more skill"
    }
}

var ADD_SKILL: String {
    get {
        return "+ Add skill"
    }
}

var SAVE_TEXT: String {
    get {
        return "Save"
    }
}

var UPDATE_TEXT: String {
    get {
        return "Update"
    }
}

var RESUME_DELETED_SUCCESSFULLY: String {
    get {
        return "Resume deleted successfully."
    }
}

var RESUME_NAME_UPDATED_SUCCESSFULLY: String {
    get {
        return "Resume name updated successfully."
    }
}

var DUPLICATE_RESUME_CREATED_SUCCESSFULLY: String {
    get {
        return "Duplicate resume created successfully."
    }
}

var NOT_SPECIFIED: String {
    get {
        return "(Not specified)"
    }
}
