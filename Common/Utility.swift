//
//  Utility.swift
//  ResumeApp
//
//  Created by Anviam on 19/08/23.
//

import UIKit

class Utility {
    static func getFormattedDate(string: String , formatter:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = formatter
        let date: Date? = dateFormatterGet.date(from: string)
        return dateFormatterPrint.string(from: date ?? Date());
    }
    
    static func getFormattedDate(date: Date , formatter:String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = formatter
        return dateFormatterPrint.string(from: date);
    }
    
    static func setErrorTextField(isError: Bool, view: UIView?, errorLabel: UILabel?, errorText: String = empty) {
        if isError {
            view?.borderColor = UIColor(borderColorForError)
            errorLabel?.textColor = UIColor(colorForError)
            view?.borderWidth = 1
            errorLabel?.text = errorText
            if view?.superview?.superview?.superview?.superview is UIScrollView {
                (view?.superview?.superview?.superview?.superview as? UIScrollView ?? UIScrollView()).scrollRectToVisible(view?.frame ?? CGRect(), animated: true)
            }
        } else {
            errorLabel?.text = empty
            view?.borderWidth = 0
        }
    }
    
}
