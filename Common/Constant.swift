//
//  Constant.swift
//  ResumeApp
//
//  Created by Anviam on 19/08/23.
//

import UIKit

// MARK: Date Format Type
let twelveHoursTimeFormat = "hh:mm a"
let twentyFourHoursTimeFormat = "HH:mm:ss"
let dateFormatOneType = "yyyy-MM-dd"
let dateFormatTwoType = "MMMM yyyy"
let dateFormatThreeType = "dd/MM/yyyy"
var dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
var empty = ""
var oneSpace = " "
var zero = 0

// Bool value of Views for using same alert.
var isCoverLetter: Bool = false
var isPersonalDetails: Bool = false
var isProfessionalSummary: Bool = false

// TextFeild error color code
let colorForError: String = "#FF3B30"
let borderColorForError: String = "#FF3B30"

